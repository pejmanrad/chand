import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../values'
import ImagePicker from 'react-native-image-crop-picker';
import {connect}from 'react-redux'
import {userSet} from '../../actions'
import { Input, Button, Avatar, Text ,Icon} from 'react-native-elements'
class Edit extends React.Component {

    showDocumentPicker = () => {
		ImagePicker.openPicker({
			width: 600,
				height: 600,
			cropping: true
		}).then(img => {
        console.log(img)
			this.props.userSet({ image: img });
		});

	};
    render() {
        const {image}=this.props;
        return (<View style={styles.container}>
            <LinearGradient
                colors={Colors.profileGradient}
                style={styles.content}
            >
                <View style={{ width: '100%', padding: 10, justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'row', }}>
					<Icon name='chevron-left' type='MaterialIcons' color='#988' size={30} onPress={() => this.props.navigation.goBack()} />

				</View>
                <TouchableOpacity onPress={this.showDocumentPicker} activeOpacity={.8}>
                    <Avatar rounded
                        size="large"
                        activeOpacity={.8}
                        showEditButton
                        containerStyle={{ marginVertical: 5 }}
                        source={image?{uri:image.path}:require('../../../assets/images/avatar_default.png')} />
                    <Text style={{ color: '#065e98', marginBottom: 30 }}>بارگذاری تصویر</Text>
                </TouchableOpacity>
                <Input inputContainerStyle={styles.inputContainer} inputStyle={styles.inputStyle} placeholder="نام کامل" />
                <View style={{ width: '80%', alignItems: 'center' }}>
                    <View style={styles.buttonsContainer}>
                        <Button
                            loading={false}
                            title={'ذخیره'}
                            containerStyle={{ marginVertical: 10 }}
                            buttonStyle={{ borderRadius: 30, backgroundColor: '#065e98' }}
                            titleStyle={{ fontSize: 15, fontWeight: 'bold', fontFamily: 'IRAN' }}
                            disabled={false}
                            raised
                        />
                    </View>
                </View>
            </LinearGradient>
        </View>)
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex: 1
    }, inputContainer: {
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        height: 45,
        borderBottomWidth: 0, marginVertical: 10
    },
    optionsContainer: {
        alignSelf: 'stretch',
        justifyContent: 'space-around',
        flexDirection: 'row-reverse',
        marginVertical: 16,

    },
    inputStyle: {
        borderColor: '#065e98', borderWidth: 1, borderRadius: 25, paddingHorizontal: 10, backgroundColor: '#fff'
    },
    ButtonContainer: {

    },
    buttonsContainer: {
        width: '80%',
        alignItems: 'stretch',
    },
})
const mapStateToProps=({user})=>{
    return {...user}
}
export default connect(mapStateToProps,{userSet})(Edit);