import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {HamraaHeader} from './commons';
import {Colors, Strings} from '../values';
import {categorySet} from '../actions';
import {ListItem} from 'react-native-elements';

class CategoryChooser extends Component {
    render() {
        const {categories, categoryModalHistory, categorySet} = this.props;
        const historyLength = categoryModalHistory.length;
        const currentCategoryList = historyLength > 0 ? categoryModalHistory[historyLength - 1] : categories;
        return (
            <View style={styles.container}>
                <HamraaHeader
                    title={Strings.titleChooseCompanyCategory}
                    iconLeft="search"
                    iconRight="arrow-forward"
                    iconLeftOnPress={() => []}
                    iconRightOnPress={() => {
                        if (historyLength > 0) {
                            categorySet({categoryModalHistory: categoryModalHistory.slice(0, historyLength - 1)});
                        } else {
                            categorySet({categoryModalVisible: false});
                        }
                    }}
                    fontSize={18}
                    containerStyle={{backgroundColor: Colors.white}}
                />
                {
                    currentCategoryList.map((item, i) => {
                        const hasSubCategory = item.subCategories.length > 0;
                        return (
                            <ListItem
                                key={i}
                                title={item.title}
                                onPress={() => {
                                    if (hasSubCategory) {
                                        categorySet({categoryModalHistory: categoryModalHistory.concat([item.subCategories])});
                                    } else {
                                        categorySet({category: item, categoryModalVisible: false});
                                    }
                                }}
                                leftIcon={hasSubCategory ? {
                                    type: 'material-community',
                                    name: 'chevron-left',
                                    color: Colors.primaryTextDarkSubLighter
                                } : null}
                                rightIcon={{
                                    type: 'material-community',
                                    name: item.icon,
                                    color: Colors.primaryTextDarkSub
                                }}
                                bottomDivider={true}
                                titleStyle={styles.title}
                                containerStyle={{paddingVertical: 12}}
                            />
                        );
                    })
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    title: {
        fontWeight: 'normal',
        fontFamily: 'IranYekanLight',
        color: Colors.primaryTextDark
    },
});

const mapStateToProps = ({general, category}) => {
    return {...general, ...category};
};

export default connect(
    mapStateToProps,
    {categorySet}
)(CategoryChooser);
