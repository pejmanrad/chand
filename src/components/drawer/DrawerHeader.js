import React, {Component} from 'react';
import {StatusBar, StyleSheet, View} from 'react-native';
import {HamraaAvatar, HamraaText} from '../commons';
import {Colors} from '../../values';

class DrawerContainer extends Component {
    render() {
        const {avatar, title, description} = this.props;
        return (
            <View style={styles.container}>
                <HamraaAvatar
                    rounded
                    size="medium"
                    title={title[0]}
                    source={{uri: avatar}}
                />
                <HamraaText style={styles.title}>
                    {title}
                </HamraaText>
                <HamraaText style={styles.description}>
                    {description}
                </HamraaText>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 170,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: StatusBar.currentHeight,
        backgroundColor: Colors.primary
    },
    title: {
        color: Colors.white,
        fontFamily: 'IranYekan'
    },
    description: {
        color: Colors.white,
        fontFamily: 'IranYekanLight',
    },
});

export default DrawerContainer;