import React, {Component} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {DrawerItems, SafeAreaView} from 'react-navigation';
import DrawerHeader from './DrawerHeader';

class DrawerContainer extends Component {
    render() {
        return (
            <ScrollView>
                <SafeAreaView style={styles.container} forceInset={{top: 'always', horizontal: 'never'}}>
                    <DrawerHeader
                        title="حمیدرضا راستی"
                        description="مدیرعامل"
                        avatar="https://avatars3.githubusercontent.com/u/43915620?s=400&u=7e017a7d706e8d8a9cb35b2bea21df52b1620c0a&v=4"
                    />
                    <DrawerItems {...this.props} />
                </SafeAreaView>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default DrawerContainer;