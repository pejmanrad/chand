import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withNavigation} from 'react-navigation';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {requestAll} from '../../actions';
import {HamraaSuggesterInput} from '../commons';
import {Strings} from '../../values';

class CustomerDashboard extends Component {

    handleItemPress = (item, index) => {
        this.props.navigation.navigate('RequestDetails', {item, index});
    };

    renderLoading() {
        if (this.props.loading) {
            return <ActivityIndicator/>;
        }
    }

    render() {
        const {all} = this.props;
        return (
            <View style={styles.container}>
                <HamraaSuggesterInput
                    inputRef={input => (this.nameInput = input)}
                    icon="magnifier"
                    placeholder={Strings.hintSearch}
                    staticData={[
                        {someAttribute: 'val1', details: {id: '1', name: 'پسته', category: 'خوراکی'}},
                        {someAttribute: 'val2', details: {id: '2', name: 'پنیر', category: 'خوراکی'}},
                        {someAttribute: 'val3', details: {id: '3', name: 'پنبه', category: 'مصرفی'}},
                        {someAttribute: 'val4', details: {id: '4', name: 'گوشت', category: 'خوراکی'}},
                        {someAttribute: 'val5', details: {id: '5', name: 'گیره', category: 'لوازم تحریر'}},
                        {someAttribute: 'val6', details: {id: '6', name: 'سیب زمینی', category: 'تره بار'}},
                        {someAttribute: 'val7', details: {id: '7', name: 'سبزه', category: 'تزیینی'}},
                    ]}
                    itemFormat={{
                        id: 'details.id',
                        name: 'details.name',
                        tags: ['details.category']
                    }}
                    containerStyle={styles.suggester}
                />
                <View style={styles.containerMain}>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    containerMain: {
        flex: 1,
        // backgroundColor: 'yellow'
    },
    suggester: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        zIndex: 1
    }

});


const mapStateToProps = ({request}) => {
    return {...request};
};

export default connect(
    mapStateToProps,
    {requestAll}
)(withNavigation(CustomerDashboard));