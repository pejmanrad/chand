import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {connect} from 'react-redux';
import {Image} from 'react-native-elements';
// import {HamraaText} from '../commons';
import {requestSet} from '../../actions';

class RequestPreview extends Component {
	
	constructor() {
		super();
		this.state = {
			uploadFile: false,

			
		};
	}
	
	render() {
		const {  title,description,requestFormFile}=this.props;
		return (
			<View style={styles.container}>
				<View style={{padding: 10, backgroudColor: '#f3f3f4', width: '80%',alignItems:'flex-end'}}>
					<Text style={styles.text}>{title}</Text>
					<Text style={styles.text}>{this.props.category.title}</Text>
					<Text style={styles.text}>{description}</Text>
					{requestFormFile?
						<View style={{justifyContent:'center',alignItems: 'center',width:'100%',marginTop:10}}>
							<Image source={{uri:requestFormFile.path}} style={{width: 150, height: 150,borderRadius:5}}/>
						</View>
					:
					null
					}
				
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	
	container: {
		paddingHorizontal: 24,
		alignItems: 'center',
	},
	text:{
		backgroundColor:'#fff',
		borderRadius:15,
		padding:10,
		textAlign:'right',
		marginBottom:10
	}
	
});


const mapStateToProps = ({request,category}) => {
	return {...request,...category};
};

export default connect(
	mapStateToProps,
	{requestSet},
)(RequestPreview);

