import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native';
import { Image } from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/FontAwesome'

class Item extends Component {
	renderBackground = (act) => {
		if (act) {
			return {
				backgroundColor: '#c2f2cc',
			};
		} else {
			return {
				backgroundColor: '#f5f5f5',
			};
		}
	};

	render() {
		const { badge, title, MaxPrice, MinPrice, ImageUri, onItemPress, active } = this.props;
		console.log(active)
		return (
			<TouchableOpacity style={styles.TouchableStyle} activeOpacity={(active&&badge)?.7:1} onPress={(active&&badge)?onItemPress:null}>

				<View style={[styles.ListItem, this.renderBackground(active)]}>
					{(badge &&active)? <View style={styles.badge}>
						<Text style={styles.badgeText}>{badge}</Text>
					</View> : null
					}
					<View style={styles.rightView}>
						<Text style={styles.title}>{title}</Text>

						{active ? 
						<View style={{ flexDirection: 'row-reverse', alignItems: 'center', justifyContent: 'flex-start' }}>
							<Icon name="check-circle-o" size={20} color="#2dc04d" />
							<Text style={{ color: '#2dc04d', marginRight: 3 }}>فعال</Text>
						</View>
							:
							<View style={{ flexDirection: 'row-reverse', alignItems: 'center', justifyContent: 'flex-start' }}>
								<Icon name="ban" size={20} color="#888" />
								<Text style={{ color: '#888', marginRight: 3 }}>غیر فعال</Text>
							</View>
						}

					</View>
					<View style={styles.centerView}>
						{active?<View style={styles.price}>
							{MaxPrice ? <Text style={styles.maxPrice}>{MaxPrice}</Text> : null}
							{MinPrice ? <Text style={styles.minPrice}>{MinPrice}</Text> : null}
						</View>:null}
					</View>
					<View style={styles.imgView}>
						<Image source={ImageUri}
							style={styles.Image} />
					</View>


				</View>
			</TouchableOpacity>

		);
	}
}

const styles = StyleSheet.create({
	ListItem: {
		flexDirection: 'row-reverse',
		marginTop: 10,
		height: 80,
		width: '90%',
		borderRadius: 10,
		padding: 12
	},
	badge: {
		height: 30, width: 30,
		backgroundColor: '#ff3b03',
		borderRadius: 15,
		position: 'absolute',
		left: -5,
		top: -5,
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: 99,
	},
	badgeText: { color: '#fff' },
	TouchableStyle: {
		width: '100%',
		alignItems: 'center',

		justifyContent: 'center',
	},
	rightView: {
		width: '40%',
		flexDirection: 'column',
		justifyContent: 'center'
	},
	centerView: {
		width: '30%',

	},
	title: {
		padding: 5,
		marginRight: 5,
		color: '#444',
		textAlign: 'right'
	},
	price: {
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'column',
	},
	maxPrice: {
		color: '#fff',
		backgroundColor: '#00cc24',
		borderRadius: 10,
		paddingLeft: 4,
		paddingRight: 4,
	},
	minPrice: {
		color: '#fff',
		marginTop: 5,
		paddingLeft: 4,
		paddingRight: 4,
		backgroundColor: '#00cc24',
		borderRadius: 10
	},
	imgView: {
		width: '30%',
		justifyContent: 'center',
		alignItems: 'flex-start',
	},
	Image: {
		width: 70,
		height: 70,
		borderRadius: 5,
	},

});

export { Item };
