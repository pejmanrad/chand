import React, {Component} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import RequestItem from './RequestItem';
import RequestItemHorizental from './RequestItemHorizental';
import {HamraaScale} from '../commons';
import PropTypes from 'prop-types';

class RequestList extends Component {
    render() {
        const {data, vertical, inverted, onItemPress, onRefresh, refreshing, containerStyle, ...otherProps} = this.props;
        return (
            <View style={[styles.container, containerStyle]}>
                <FlatList
                    data={data}
                    keyExtractor={(item) => String(item.id)}
                    contentContainerStyle={vertical ? styles.vertical : styles.horizontal}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    horizontal={!vertical}
                    inverted={inverted}
                    onRefresh={onRefresh}
                    refreshing={refreshing}
                    renderItem={
                        ({item, index}) => {
                            return (
                                <HamraaScale onPress={() => onItemPress(item, index)}>
                                    {vertical ? <RequestItem request={item} index={index}/> :
                                        <RequestItemHorizental request={item} index={index}/>}
                                </HamraaScale>
                            );
                        }
                    }
                    {...otherProps}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    vertical: {
        paddingVertical: 8
    },
    horizontal: {
        paddingHorizontal: 8
    },
});

RequestList.propTypes = {
    data: PropTypes.array,
    onItemPress: PropTypes.func,
    onRefresh: PropTypes.func,
    vertical: PropTypes.bool,
    inverted: PropTypes.bool,
    refreshing: PropTypes.bool,
};

RequestList.defaultProps = {
    vertical: true,
    inverted: false,
    refreshing: false,
};

export default RequestList;