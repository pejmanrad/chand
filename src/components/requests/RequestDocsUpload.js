import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Platform, Text, TouchableOpacity } from 'react-native';
import { Icon, Image } from 'react-native-elements';
import ImagePicker from 'react-native-image-crop-picker';
import { Colors, Strings } from '../../values';
import { requestSet } from '../../actions';


class RequestDocsUpload extends Component {
	constructor() {
		super();
		this.state = {
			uploadFile: false,
			image: require('../../../assets/splash.png'),

		};
	}
	cameraPicker = () => {
		ImagePicker.openCamera({
			width: 600,
			height: 600,
			cropping: true,
		}).then(image => {
			this.props.requestSet({ requestFormFile: image });
			console.log(image);
		});
	}
	showDocumentPicker = () => {
		ImagePicker.openPicker({
			width: 600,
				height: 600,
			cropping: true
		}).then(img => {
			this.props.requestSet({ requestFormFile: img });
			console.log(img);
		});

	};


	render() {
		const {
			requestFormFile,
		} = this.props;
		return (
			<View style={styles.container}>
				{requestFormFile ?
					<View>
						<View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 5, padding: 5 }}>
							<Image source={{ uri: requestFormFile.path }} style={{ width: 150, height: 150, borderRadius: 5 }} />
							<Text style={{ marginTop: 15, color: Colors.primaryActive }}>تصویر با موفقیت آپلود شد.</Text>
						</View>
					</View>
					:
					<View style={{ justifyContent: 'center', alignItems: 'center' }}>
						<TouchableOpacity onPress={this.cameraPicker}>

							<View style={{
								padding: 10,
								borderRadius: 35,
								justifyContent: 'center',
								alignItems: 'center',
								borderWidth: 1,
								borderColor: '#ccc',
							}}>
								<Icon name="camera" type="MaterialIcons" color={Colors.primaryActive} size={70} />
							</View>
							<Text style={{ marginTop: 10, color: Colors.primaryActive }}>گرفتن تصویر کالای درخواستی</Text>
						</TouchableOpacity>

						<TouchableOpacity onPress={this.showDocumentPicker}>

							<View style={{
								padding: 10,
								borderRadius: 35,
								justifyContent: 'center',
								alignItems: 'center',
								borderWidth: 1,
								borderColor: '#ccc',
							}}>
								<Icon name="file-upload" type="MaterialIcons" color={Colors.primaryActive} size={70} />
							</View>
							<Text style={{ marginTop: 10, color: Colors.primaryActive }}>آپلود تصویر کالای درخواستی</Text>
						</TouchableOpacity>
					</View>

				}

			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 24,
		justifyContent: 'center',
		alignItems: 'center',
	},

});

const mapStateToProps = ({ request }) => {
	return { ...request };
};

export default connect(
	mapStateToProps,
	{ requestSet},
)(RequestDocsUpload);
