import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Image} from 'react-native-elements';
import {Colors, Dimensions, Strings} from '../../values';
import {HamraaText} from '../commons';

class RequestItem extends Component {
    render() {
        const {request} = this.props;
        const image = request.thumbnail ? {uri: request.thumbnail} : require('../../../assets/images/image_default.png');
        return (
            <View style={styles.container}>
                <View style={styles.containerLeft}>
                    <View>
                        <HamraaText style={styles.title}>{request.title}</HamraaText>
                        <HamraaText style={styles.description}>{request.description}</HamraaText>
                    </View>
                    <HamraaText style={{...styles.description, color: Colors.primaryTextDark,}}>
                        {`${Strings.titleExpiresAt} ${request.expiresAt}`}
                    </HamraaText>
                </View>
                <View style={styles.containerRight}>
                    <Image
                        style={styles.image}
                        containerStyle={styles.image}
                        source={image}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 4,
        marginHorizontal: 8,
        flexDirection: 'row',
        borderRadius: 24,
        backgroundColor: Colors.primaryLight
    },
    containerLeft: {
        flex: 1,
        padding: 8,
        justifyContent: 'space-around'
    },
    containerRight: {
        // flex: 0.35,
    },
    image: {
        width: 144,
        height: 144,
        borderTopRightRadius: 24,
        borderBottomRightRadius: 24,
    },
    title: {
        fontFamily: 'IranYekan',
        color: Colors.primaryTextDark,
    },
    description: {
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMedium,
        color: Colors.primaryTextDarkSub,
    }
});

export default RequestItem;