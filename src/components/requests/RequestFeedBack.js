import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
	ActivityIndicator,
	StyleSheet,
	View,
	FlatList,
	SafeAreaView,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {offerAll, offerShow, offerMore} from '../../actions';
import {HeaderGradiant, PRList} from '../commons';
import {OfferItem} from '../commons';

class RequestFeekBack extends Component {
	
	showOfferDetails = (id) => {
		this.props.offerShow(id);
	};
	moreData = () => {
		const {pagination, loading, all, page} = this.props;
		if (!loading && (all.length <= 0 || pagination.current < pagination.last)) {
			const offerPage = page + 1;
			this.props.offerMore({offerPage});
		}
	};
	
	
	render() {
		const {fetchLoading, loading, all} = this.props;
		return (
			<SafeAreaView style={styles.container}>
				<HeaderGradiant
					title='لیست پیشنهادات'
					rightIcon={<Icon name='menu' type='MaterialIcons' color='#fff' size={26}/>}
					leftIcon={<Icon name='chevron-left' type='MaterialIcons' color='#fff' size={26}
										 onPress={() => this.props.navigation.goBack()}/>}
				/>
				<View style={styles.container}>
					{loading ?
						<ActivityIndicator size={'large'}/>
						:
						<PRList
							data={all}
							listFooter={fetchLoading ? () => <ActivityIndicator size={'large'}/> : null}
							endReached={this.moreData}
							threshold={.01}
							renderItem={({item}) => {
								return <OfferItem title={item.title}
														price={item.price}
														onPress={() => this.showOfferDetails(item.id)}/>;
							}}
						/>
					}
				
				</View>
			</SafeAreaView>
		);
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingBottom: 10,
	},
	GradiantStyle: {
		marginTop: 35,
		flex: 1,
		borderTopRightRadius: 50,
		borderTopLeftRadius: 50,
		paddingTop: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},
	item: {
		width: '90%',
		backgroundColor: '#fff',
		borderRadius: 20,
		padding: 10,
	},
});

const mapStateToProps = ({offer}) => {
	return {...offer};
};
export default connect(mapStateToProps, {offerAll, offerShow, offerMore})(RequestFeekBack);
