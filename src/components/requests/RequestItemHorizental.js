import React, {Component} from 'react';
import {ImageBackground, StyleSheet, View} from 'react-native';
import {Colors, Dimensions} from '../../values';
import {HamraaText} from '../commons';

class RequestItemHorizental extends Component {
    render() {
        const {request, index} = this.props;
        const image = request.thumbnail ? {uri: request.thumbnail} : require('../../../assets/images/image_default.png');
        let firstItemStyle = {};
        if (index === 0) {
            firstItemStyle.marginRight = 16;
        }
        return (
            <View style={{...styles.container, ...firstItemStyle}}>
                <ImageBackground
                    style={styles.image}
                    imageStyle={styles.image}
                    source={image}
                >
                    <HamraaText style={styles.title}>{request.title}</HamraaText>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 24,
        marginVertical: 4,
        marginHorizontal: 8,
        backgroundColor: Colors.primaryLight
    },
    containerLeft: {
        flex: 1,
        padding: 8,
        justifyContent: 'space-around'
    },
    containerRight: {
        // flex: 0.35,
    },
    image: {
        width: 180,
        height: 180,
        borderRadius: 24,
        justifyContent: 'flex-end',
    },
    title: {
        padding: 4,
        fontFamily: 'IranYekan',
        textAlign:'center',
        color: Colors.primaryTextLight,
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24,
        backgroundColor: 'rgba(0, 0, 0, 0.24)',
    },
    description: {
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMedium,
        color: Colors.primaryTextDarkSub,
    }
});

export default RequestItemHorizental;