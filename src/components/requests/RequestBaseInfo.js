import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { HamraaButtonGroup, HamraaModal, HamraaTextInput } from '../commons';
import { Strings } from '../../values';
import { categorySet, requestSet } from '../../actions';
import CategoryChooser from '../CategoryChooser';

class RequestBaseInfo extends Component {
	constructor(props) {
		super(props);
		this.state={
			title:'',
			description:'',
			category:{}
		}
	}
	changeTitle=(title)=>{
		this.setState({title})
		this.props.requestSet({title})
	}
	changeDescription=(description)=>{
		this.setState({description});
		this.props.requestSet({description})
	}
	render() {
		const {
			titleValid,
			descriptionValid,
			category,
			categoryValid,
			categoryModalVisible,
			categorySet,
		} = this.props;
		return (
			<View style={styles.container}>

				<HamraaTextInput
					inputRef={input => {
						this.titleInput = input;
						this.props.setRefs({ titleInput: input });
					}}
					value={this.state.title}
					onChangeText={title => this.changeTitle(title)}
					placeholder={Strings.hintTitle}
					keyboardType="default"
					returnKeyType="next"
					errorMessage={
						titleValid ? null : Strings.errorInvalidTitle
					}
					onSubmitEditing={() => {

					}}
				/>
				<HamraaTextInput
					inputRef={input => {
						this.descriptionInput = input;
						this.props.setRefs({ descriptionInput: input });
					}}

					value={this.state.description}
					onChangeText={description => this.changeDescription(description)}
					placeholder={Strings.hintDescription}
					keyboardType="default"
					returnKeyType="next"
					muluti={true}
					containerStyle={{ height: 84 }}
					errorMessage={
						descriptionValid ? null : Strings.errorInvalidDescription
					}
					onSubmitEditing={() => {

					}}
				/>
				<TouchableOpacity
					style={{ alignSelf: 'stretch' }}
					onPress={() => categorySet({ categoryModalVisible: true })}
				>
					<HamraaTextInput
						inputRef={input => {
							this.categoryInput = input;
							this.props.setRefs({ categoryInput: input });
						}}

						 value={category.title}
						// onChangeText={category => this.setState({})}
						placeholder={Strings.hintChoose}
						// onTouchStart={() => console.warn('category chooser')}
						editable={false}
						pointerEvents="none"
						keyboardType="default"
						returnKeyType="next"
						errorMessage={
							categoryValid ? null : Strings.errorInvalidCompanyCategory
						}
						onSubmitEditing={() => {

						}}
					/>
				</TouchableOpacity>

				<HamraaModal
					visible={categoryModalVisible}
					onBackdropPress={() => categorySet({ categoryModalVisible: false })}
				>
					<CategoryChooser />
				</HamraaModal>
			</View>
		);
	}
}

const styles = StyleSheet.create({

	container: {
		paddingHorizontal: 24,
	},

});


const mapStateToProps = ({ request, category }) => {
	return { ...request, ...category };
};

export default connect(
	mapStateToProps,
	{ requestSet, categorySet },
)(RequestBaseInfo);
