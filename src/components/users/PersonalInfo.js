import React, {Component} from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import {HamraaAvatar, HamraaTextInput} from '../commons';
import {Strings} from '../../values';

class PersonalInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            birthdate: '',
            nameValid: true,
            birthdateValid: true,
        };
    }

    validateName() {
        const {name} = this.state;
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const nameValid = re.test(name);
        // LayoutAnimation.easeInEaseOut();
        this.setState({nameValid});
        nameValid || this.nameInput.shake();
        return nameValid;
    }

    validateBirthdate() {
        const {birthdate} = this.state;
        const birthdateValid = birthdate.length >= 8;
        // LayoutAnimation.easeInEaseOut();
        this.setState({birthdateValid});
        birthdateValid || this.birthdateInput.shake();
        return birthdateValid;
    }

    render() {
        const {
            name,
            nameValid,
            birthdate,
            birthdateValid,
        } = this.state;
        return (
            <ScrollView
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={styles.container}
            >
                <View style={{width: '80%', alignItems: 'center', justifyContent: 'center'}}>

                    <HamraaAvatar
                        rounded
                        size="large"
                        showEditButton
                        containerStyle={{marginBottom: 36}}
                        source={require('../../../assets/images/avatar_default.png')}
                    />

                    <HamraaTextInput
                        inputRef={input => this.nameInput = input}
                        icon="user"
                        title={Strings.titleName}
                        value={name}
                        onChangeText={name => this.setState({name})}
                        placeholder={Strings.hintName}
                        returnKeyType="next"
                        errorMessage={
                            nameValid ? null : Strings.errorInvalidName
                        }
                        onSubmitEditing={() => {
                            this.validateName();
                            this.birthdateInput.focus();
                        }}
                    />
                    <HamraaTextInput
                        inputRef={input => this.birthdateInput = input}
                        icon="calendar"
                        title={Strings.titleBirthdate}
                        value={birthdate}
                        onChangeText={birthdate => this.setState({birthdate})}
                        placeholder={Strings.hintDate}
                        returnKeyType="next"
                        errorMessage={
                            birthdateValid ? null : Strings.errorInvalidBirthdate
                        }
                        onSubmitEditing={() => {
                            this.validateBirthdate();
                        }}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    formContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    image: {
        marginBottom: 20,
        height: 100,
        width: 100,
    },
});

export default PersonalInfo;