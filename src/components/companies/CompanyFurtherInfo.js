import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {HamraaTextInput} from '../commons';
import {Strings} from '../../values';
import {companySet} from '../../actions';

class CompanyFurtherInfo extends Component {

    renderNaturalPerson() {
        const {
            natural,
            companySet,
            companyType,
        } = this.props;
        const {
            businessLicenseNumber,
            businessLicenseNumberValid,
            tradeUnionId,
            tradeUnionIdValid,
        } = natural;
        if (companyType === 1) {
            return (
                <View>
                    <HamraaTextInput
                        inputRef={input => this.businessLicenseNumberInput = input}
                        icon="doc"
                        label={Strings.titleBusinessLicenseNumber}
                        value={businessLicenseNumber}
                        onChangeText={businessLicenseNumber => companySet({
                            natural: {
                                ...natural,
                                businessLicenseNumber
                            }
                        })}
                        placeholder={Strings.hintNumber}
                        keyboardType="default"
                        returnKeyType="next"
                        errorMessage={
                            businessLicenseNumberValid ? null : Strings.errorInvalidBusinessLicenseNumber
                        }
                        onSubmitEditing={() => {

                        }}
                    />
                    <HamraaTextInput
                        inputRef={input => this.tradeUnionIdInput = input}
                        icon="people"
                        label={Strings.titleTradeUnionId}
                        value={tradeUnionId}
                        onChangeText={tradeUnionId => companySet({natural: {...natural, tradeUnionId}})}
                        placeholder={Strings.hintId}
                        returnKeyType="next"
                        errorMessage={
                            tradeUnionIdValid ? null : Strings.errorInvalidPostalCode
                        }
                        onSubmitEditing={() => {

                        }}
                    />
                </View>
            );
        }
    }

    renderLegalPerson() {
        const {
            legal,
            companySet,
            companyType,
        } = this.props;
        const {
            nationalId,
            nationalIdValid,
            registerationNumber,
            registerationNumberValid,
        } = legal;
        if (companyType === 0) {
            return (
                <View>
                    <HamraaTextInput
                        inputRef={input => this.registerationNumberInput = input}
                        icon="doc"
                        label={Strings.titleRegisterationNumber}
                        value={registerationNumber}
                        onChangeText={registerationNumber => companySet({legal: {...legal, registerationNumber}})}
                        placeholder={Strings.hintNumber}
                        keyboardType="default"
                        returnKeyType="next"
                        errorMessage={
                            registerationNumberValid ? null : Strings.errorInvalidRegisterationNumber
                        }
                        onSubmitEditing={() => {

                        }}
                    />
                    <HamraaTextInput
                        inputRef={input => this.nationalIdInput = input}
                        icon="info"
                        label={Strings.titleNationalId}
                        value={nationalId}
                        onChangeText={nationalId => companySet({legal: {...legal, nationalId}})}
                        placeholder={Strings.hintId}
                        returnKeyType="next"
                        errorMessage={
                            nationalIdValid ? null : Strings.errorInvalidNationalId
                        }
                        onSubmitEditing={() => {

                        }}
                    />
                </View>
            );
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderNaturalPerson()}
                {this.renderLegalPerson()}
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        paddingHorizontal: 24,
    },

});


const mapStateToProps = ({company}) => {
    return {...company};
};

export default connect(
    mapStateToProps,
    {companySet}
)(CompanyFurtherInfo);