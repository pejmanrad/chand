import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View, Platform} from 'react-native';
import {Icon, ListItem} from 'react-native-elements';
// noinspection ES6CheckImport
import {DocumentPicker, DocumentPickerUtil,} from 'react-native-document-picker';
import {HamraaProgress} from '../commons';
import {Colors, Strings} from '../../values';
import {companySet} from '../../actions';

class CompanyDocsUpload extends Component {

    showDocumentPicker = (natural) => {
        DocumentPicker.show({
            filetype: [DocumentPickerUtil.images()],
        }, (error, res) => {
            // Android
            Platform.select({
                ios: () => {
                },
                android: () => {
                    if (res !== null) {
                        this.props.companySet({natural: {...natural, businessLicenseFile: {...res}}});
                    }
                },
            })();
        });
    };

    renderNaturalPerson() {
        const {
            natural,
            companySet,
            companyType,
        } = this.props;
        const {
            businessLicenseFile,
            businessLicenseUploading,
            businessLicenseUploadingProgress,
        } = natural;
        if (companyType === 1) {
            return (
                <View>
                    <ListItem
                        title={
                            businessLicenseFile ? businessLicenseFile.fileName : Strings.titleBusinessLicense
                        }
                        subtitle={
                            <HamraaProgress progress={businessLicenseUploadingProgress}/>
                        }
                        leftIcon={
                            businessLicenseUploading === true ?
                                {
                                    type: 'material-community',
                                    name: 'close',
                                    color: Colors.primaryTextDarkSub
                                }
                                : businessLicenseUploading === false ?
                                {
                                    type: 'material-community',
                                    name: 'check',
                                    color: Colors.primary
                                }
                                : null
                        }
                        rightIcon={
                            <Icon
                                raised
                                reverse
                                name='file-document-outline'
                                type='material-community'
                                onPress={() => this.showDocumentPicker(natural)}
                                color={Colors.primaryActive}
                            />
                        }
                        titleStyle={styles.title}
                        containerStyle={styles.item}
                    />
                </View>
            );
        }
    }

    renderLegalPerson() {
        const {
            legal,
            companySet,
            companyType,
        } = this.props;
        const {
            nationalId,
            registerationNumber,
        } = legal;
        if (companyType === 0) {
            return (
                <View>
                </View>
            );
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderNaturalPerson()}
                {this.renderLegalPerson()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 24,
    },
    item: {},
    title: {
        textAlign: 'right',
        fontWeight: 'normal',
        fontFamily: 'IranYekanLight',
        color: Colors.primaryTextDark,
    },
});

const mapStateToProps = ({company}) => {
    return {...company};
};

export default connect(
    mapStateToProps,
    {companySet}
)(CompanyDocsUpload);