import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {HamraaButtonGroup, HamraaModal, HamraaTextInput} from '../commons';
import {Strings} from '../../values';
import {companySet, categorySet} from '../../actions';
import CategoryChooser from '../CategoryChooser';

class CompanyBaseInfo extends Component {

    render() {
        const {
            companyType,
            name,
            nameValid,
            category,
            categoryValid,
            postalCode,
            postalCodeValid,
            bankAccountNumber,
            bankAccountNumberValid,
            categoryModalVisible,
            companySet,
            categorySet
        } = this.props;
        return (
            <View style={styles.container}>
                <HamraaButtonGroup
                    buttons={[Strings.titleLegalPerson, Strings.titleNaturalPerson]}
                    onPress={companyType => companySet({companyType})}
                    selectedIndex={companyType}
                />
                <HamraaTextInput
                    inputRef={input => this.nameInput = input}
                    icon="home"
                    label={Strings.titleCompanyName}
                    value={name}
                    onChangeText={name => companySet({name})}
                    placeholder={Strings.hintName}
                    keyboardType="default"
                    returnKeyType="next"
                    errorMessage={
                        nameValid ? null : Strings.errorInvalidName
                    }
                    onSubmitEditing={() => {

                    }}
                />
                <TouchableOpacity
                    style={{alignSelf: 'stretch'}}
                    onPress={() => categorySet({categoryModalVisible: true})}
                >
                    <HamraaTextInput
                        inputRef={input => this.categoryInput = input}
                        icon="list"
                        label={Strings.titleCompanyCategory}
                        value={category}
                        onChangeText={category => categorySet({category})}
                        placeholder={Strings.hintChoose}
                        // onTouchStart={() => console.warn('category chooser')}
                        editable={false}
                        pointerEvents="none"
                        keyboardType="default"
                        returnKeyType="next"
                        errorMessage={
                            categoryValid ? null : Strings.errorInvalidCompanyCategory
                        }
                        onSubmitEditing={() => {
                        }}
                    />
                </TouchableOpacity>
                <HamraaTextInput
                    inputRef={input => this.postalCodeInput = input}
                    icon="envelope-letter"
                    label={Strings.titlePostalCode}
                    value={postalCode}
                    onChangeText={postalCode => companySet({postalCode})}
                    placeholder={Strings.hintPostalCode}
                    returnKeyType="next"
                    errorMessage={
                        postalCodeValid ? null : Strings.errorInvalidPostalCode
                    }
                    onSubmitEditing={() => {

                    }}
                />
                <HamraaTextInput
                    inputRef={input => this.bankAccountNumberInput = input}
                    icon="credit-card"
                    label={Strings.titleBankAccountNumber}
                    value={bankAccountNumber}
                    onChangeText={bankAccountNumber => companySet({bankAccountNumber})}
                    placeholder={Strings.hintBankAccountNumber}
                    returnKeyType="next"
                    errorMessage={
                        bankAccountNumberValid ? null : Strings.errorInvalidBankAccountNumber
                    }
                    onSubmitEditing={() => {

                    }}
                />
                <HamraaModal
                    visible={categoryModalVisible}
                    onBackdropPress={() => categorySet({categoryModalVisible: false})}
                >
                    <CategoryChooser/>
                </HamraaModal>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        paddingHorizontal: 24,
    },

});


const mapStateToProps = ({company, category}) => {
    return {...company, ...category};
};

export default connect(
    mapStateToProps,
    {companySet, categorySet}
)(CompanyBaseInfo);
