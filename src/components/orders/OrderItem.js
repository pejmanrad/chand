import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Animated } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class OrderItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false
		}
	}
	UNSAFE_componentWillMount() {
		this.animatedValue1 = new Animated.Value(0);
		this.animatedValue2 = new Animated.Value(.4);
		this.animatedValue3 = new Animated.Value(.6);
	}
	ShowButton() {
		if (this.props.status === 'pending') {
			Animated.stagger(300, [
				Animated.timing(this.animatedValue3, {
					toValue: .4, duration: 100,
				}),
				Animated.timing(this.animatedValue2, {
					toValue: .6, duration: 100
				}),

			]).start()
			setTimeout(() => {
				this.props.onItemPress();
				Animated.stagger(300, [
					Animated.timing(this.animatedValue3, {
						toValue: .6, duration: 100,
					}),
					Animated.timing(this.animatedValue2, {
						toValue: .4, duration: 100
					}),

				]).start()
			}, 700);
		}
	}
	render() {
		const animatedStyle1 = {
			left: this.animatedValue1.interpolate({
				inputRange: [0, 1],
				outputRange: ['0%', '100%']
			})
		}
		const animatedStyle2 = {
			width: this.animatedValue2.interpolate({
				inputRange: [0, 1],
				outputRange: ['0%', '100%']
			})
		}
		const animatedStyle3 = {
			width: this.animatedValue3.interpolate({
				inputRange: [0, 1],
				outputRange: ['0%', '100%']
			})
		}
		const { title, onItemPress, status } = this.props;
		return (<TouchableOpacity style={styles.TouchableStyle} activeOpacity={.8} onPress={() => this.ShowButton()} >
			<View style={styles.ListItem}>
				<Animated.View style={[styles.rightView, animatedStyle3]}>
					<Text style={styles.title}>{title}</Text>
				</Animated.View>
				<Animated.View style={[styles.stepView, animatedStyle2]}>
					{status === "pending" ?
						<View style={{ width: '100%', height: '100%', borderRadius: 10, backgroundColor: '#00cb29', justifyContent: 'center', alignItems: 'center' }}>
							<Icon
								name='check-circle-o'
								color='#fff'
								size={25}
							/>
							<Text style={{ color: '#fff' }}>تحویل داده شد</Text>
						</View>
						:
						<View style={[styles.transform]}>
							<Icon
								name='truck'
								color='#fff'
								size={25}
							/>
							<Text style={{ color: '#fff' }}>در حال ارسال</Text>
						</View>
					}


				</Animated.View>
			</View>
		</TouchableOpacity>);
	}
}

const styles = StyleSheet.create({
	ListItem: {
		flexDirection: 'row-reverse',
		marginTop: 10,
		height: 80,
		backgroundColor: '#e3dede',
		width: '90%',
		borderRadius: 10,

	},

	TouchableStyle: {
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
	},
	rightView: {

		justifyContent: 'center',
		alignItems: 'flex-end', paddingRight: 15
	},
	title: {
		color: '#406784',
		padding: 5,
		borderRadius: 5,
	},
	transform: {
		position: 'relative',
		width: '100%',
		height: '100%',
		borderRadius: 10,
		backgroundColor: '#ff9000',
		justifyContent: 'center',
		alignItems: 'center'
	},
	price: {
		justifyContent: 'flex-end',
		alignItems: 'center',
		flexDirection: 'row',
	},
	maxPrice: {
		color: '#c60000',
	},
	minPrice: {
		color: 'green',
	},
	stepView: {

		justifyContent: 'center',
		alignItems: 'center',
	},
	Image: {
		width: 70,
		height: 70,
		borderRadius: 5,
	},

});
export { OrderItem };
