import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Text } from 'react-native-elements'
import { Icon, Input } from 'react-native-elements';
import MyIcon from 'react-native-vector-icons/dist/FontAwesome'
import { HeaderGradiant,HamraaButton } from '../commons';
class OrderView extends React.Component {
    constructor() {
        super();
        this.state = {
            defaultRating: 3,
            maxRting: 5,
        }
    }
    updateRating(key) {
        this.setState({ defaultRating: key })
    }
    render() {
        let ReactNativeRatingBar = []
        for (let i = 1; i <= this.state.maxRting; i++) {
            ReactNativeRatingBar.push(
                <TouchableOpacity key={i}
                    activeOpacity={.7}
                    onPress={this.updateRating.bind(this, i)}
                >
                    <MyIcon name={(i <= this.state.defaultRating) ? 'star' : 'star-o'}  color={(i <= this.state.defaultRating) ? '#ffb300' : '#ccc'} size={30} />
                </TouchableOpacity>
            )
        }
        const item = this.props.navigation.getParam('item')
        return (<View style={styles.container}>
            <HeaderGradiant
                title='وضعیت سفارش شما'
                rightIcon={<Icon name='menu' type='MaterialIcons' color='#fff' size={26} />}
                leftIcon={<Icon name='chevron-left' type='MaterialIcons' color='#fff' size={26}
                    onPress={() => this.props.navigation.goBack()} />} />
            <View style={styles.content}>
                <View style={{ paddingTop: 20, paddingBottom: 20, alignItems: 'center', justifyContent: 'center' }}>
                    <MyIcon name="check-circle-o" color="#00cb29" size={35} />
                    <Text style={{ color: '#00cb29', fontSize: 15 }}>سفارش شما رسید</Text>
                </View>
                <View style={{ paddingTop: 10, paddingBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 15, fontFamily: 'IRAN', marginBottom: 10 }}>به فروشنده امتیاز بدهید</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: 180 }}>
                        {ReactNativeRatingBar}
                    </View>
                </View>

                <View style={{ paddingTop: 10, paddingBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 15, fontFamily: 'IRAN', marginBottom: 10 }}>نقاط ضعف و قوت فروشنده</Text>
                  <Input 
                  multiline={true}
                  inputStyle={styles.inputStyle}
                  containerStyle={{width:'70%'}}
                  inputContainerStyle={{borderBottomWidth:0}} 
                  numberOfLines={4}
                  />
                </View>
                <View style={{ alignItems: 'center'}}>
							<View style={{	width: '80%',alignItems: 'stretch'}}>
								<HamraaButton
									title={'تایید'}
									loading={false}
									onPress={()=>alert('hi')}/>
							</View>
				</View>
            </View>
        </View>)
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    content: {
        flexDirection: 'column',
    },
    inputStyle:{
        borderWidth:1,

        textAlign:'right',
        borderColor:'#ccc',
        borderRadius:5,
        width:'70%',

    }
})
export default OrderView;

