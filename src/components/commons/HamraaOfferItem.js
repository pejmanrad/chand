import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Colors} from '../../values'
class OfferItem extends React.Component {
	
	render() {
		const {price, title, onPress} = this.props;
		
		return (
			
			<View style={styles.view}>
				<Text style={styles.text}>{title}</Text>
				<Text style={styles.text}>{price}</Text>
				<TouchableOpacity style={styles.touchable} onPress={onPress}>
					<Text style={styles.text}>نمایش</Text>
				</TouchableOpacity>
			</View>
		
		);
	}
}

const styles = StyleSheet.create({
	view: {
		flexDirection: 'row-reverse',
		justifyContent: 'space-between',
		padding:10,
		marginTop:10,
		marginRight:10,
		marginLeft:10,
		alignItems:'center',
		borderWidth:1,borderRadius:5,borderColor:'gray'
	},
	text:{
		justifyContent:'flex-start'
	},
	touchable:{
		backgroundColor:Colors.green,
		padding:10,
		borderRadius:5
	}
});
export {OfferItem};
