import React, {Component} from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';

class GridViewComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			counter:0,
			data: [
				{
					id: 1,
					title: 'لوازم یدکی',
					sub: {
						id: 5,
						title: 'پراید',
					},
				},
				{
					id: 2,
					title: 'غذا',
				},
			],
		};
	}
	
	renderView=({item})=> {
		return (<View style={styles.grid}>
		
		</View>);
	};
	
	render() {
		return (
			<View style={{flex: 1}}>
				<FlatList
					data={this.state.data}
					keyExtractor={item => item.id.toString()}
					renderItem={({item,index}) => {
						
						return <Text>{item.title}</Text>;
					}}
			/>
	
	</View>);
	}
	
	
}

const styles = StyleSheet.create({
	row: {
		flexDirection: 'row', justifyContent: 'space-around', padding: 10,
	},
	grid: {
		backgroundColor: 'red',
		width: '40%',
		height: 100,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 10,
	},
});

export {GridViewComponent};
