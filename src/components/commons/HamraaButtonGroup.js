import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {ButtonGroup} from 'react-native-elements';
import {Colors, Dimensions} from '../../values';

class HamraaButtonGroup extends Component {
    
    render() {
        const {
            buttons, selectedIndex, textStyle, buttonStyle, containerStyle, onPress, ...otherProps
        } = this.props;
        return (
            <ButtonGroup
                buttons={buttons}
                onPress={onPress}
                selectedIndex={selectedIndex}
                containerBorderRadius={22.5}
                textStyle={{...styles.text, ...textStyle}}
                selectedTextStyle={styles.textSelected}
                buttonStyle={{...styles.button, ...buttonStyle}}
                selectedButtonStyle={styles.buttonSelected}
                containerStyle={{...styles.container, ...containerStyle}}
                {...otherProps}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 45,
        borderRadius: 22.5,
    },
    button: {},
    buttonSelected: {
        backgroundColor: Colors.primary
    },
    text: {
        fontFamily: 'IranYekan',
        fontSize: Dimensions.fontMediumXLarge,
        color: Colors.primaryTextDarkSub
    },
    textSelected: {
        fontFamily: 'IranYekan',
        fontSize: Dimensions.fontMediumXLarge,
        color: Colors.primaryTextLight
    },
});

export {HamraaButtonGroup} ;
