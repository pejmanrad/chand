import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Overlay} from 'react-native-elements';
import {Colors, Dimensions} from '../../values';

class HamraaModal extends Component {
    render() {
        const {
            borderRadius,
            children,
            fullScreen,
            onBackdropPress,
            visible,
            ...otherProps
        } = this.props;
        return (
            <Overlay
                borderRadius={borderRadius}
                fullScreen={fullScreen}
                isVisible={visible}
                onBackdropPress={onBackdropPress}
                {...otherProps}
            >
                {children}
            </Overlay>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 45,
        borderRadius: 22.5,
    },
    button: {},
    buttonSelected: {
        backgroundColor: Colors.primaryDark
    },
    text: {
        fontFamily: 'IranYekan',
        fontSize: Dimensions.fontMediumXLarge,
        color: Colors.primaryTextDarkSub
    },
    textSelected: {
        fontFamily: 'IranYekan',
        fontSize: Dimensions.fontMediumXLarge,
        color: Colors.primaryTextLight
    },
});

export {HamraaModal} ;
