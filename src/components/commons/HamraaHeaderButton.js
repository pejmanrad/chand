import React, {Component} from 'react';
import {HeaderButton} from 'react-navigation-header-buttons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors} from '../../values';

class HamraaHeaderButton extends Component {
    render() {
        return (
            <HeaderButton
                iconSize={24}
                color={Colors.primaryDark}
                IconComponent={MaterialCommunityIcons}
                {...this.props}
            />
        );
    }
}

export {HamraaHeaderButton};