import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import{HamraaTextInputLinear,HamraaTextInput} from './index'
import {connect} from 'react-redux';
import {categorySet} from '../../actions';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {FlatGrid} from 'react-native-super-grid';
import {Image, ListItem} from 'react-native-elements';
import {Colors, Strings} from '../../values';
import {HamraaHeader} from './HamraaHeader';
import {withNavigation} from 'react-navigation';

class GridView extends Component {
	
	
	render() {
		const {categories, categoryModalHistory, categorySet} = this.props;
		const historyLength = categoryModalHistory.length;
		const currentCategoryList = historyLength > 0 ? categoryModalHistory[historyLength - 1] : categories;
		console.log(categoryModalHistory);
		return (
			<View style={styles.gridView}>
				<View style={{flexDirection:'row-reverse',justifyContent:'center'}}>
					<View style={{width:'80%'}}>
						<HamraaTextInput
							icon="search"
							iconSize={25}
							placeholder={Strings.hintSearch}
							keyboardType="default"
							
						/>
					</View>
					<View style={{width:'20%',justifyContent:'center',alignItems:'center'}}>
						{(historyLength > 0) ?
							<TouchableOpacity activeOpacity={.8}
							
													onPress={() => {
														categorySet({categoryModalHistory: categoryModalHistory.slice(0, historyLength - 1)});
													}}>
								<Icon name={'long-arrow-left'} size={30} color={'#fff'}/>
							</TouchableOpacity>
							:
							<Icon name={'tasks'} size={30} color={'#fff'}/>
						}
					</View>
				</View>
				
				<FlatGrid
					itemDimension={110}
					items={currentCategoryList}
					//	style={}
					// staticDimension={300}
					// fixed
					spacing={20}
					renderItem={({item, index}) => (
						<TouchableOpacity
							activeOpacity={.8}
							onPress={() => {
								if (item.subCategories.length > 0) {
									categorySet({categoryModalHistory: categoryModalHistory.concat([item.subCategories])});
								} else {
									
									categorySet({category: item, categoryModalVisible: false});
									this.props.navigation.navigate('RequestCreate');
								}
							}}>
							<View style={[styles.itemContainer, {backgroundColor: '#127aaf'}]}>
								<Text style={styles.itemName}>{item.title}</Text>
							</View>
						</TouchableOpacity>
					
					)}
				/>
			</View>
		
		);
	}
}

const styles = StyleSheet.create({
	container: {
		marginTop: 10,
		padding: 5,
	}
	, gridView: {
		marginTop: 20,
		flex: 1,
	},
	itemContainer: {
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 10,
		padding: 10,
		height: 150,
	},
	itemName: {
		fontSize: 16,
		color: '#fff',
		fontWeight: '600',
	},
	itemCode: {
		fontWeight: '600',
		fontSize: 12,
		color: '#fff',
		textAlign: 'center',
	},
});
const mapStateToProps = ({general, category}) => {
	return {...general, ...category};
};

export default connect(mapStateToProps, {categorySet})(withNavigation(GridView));
