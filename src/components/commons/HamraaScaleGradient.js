import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import TouchableScale from 'react-native-touchable-scale';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import {HamraaText} from './HamraaText';
import {Colors, Dimensions} from '../../values';

class HamraaScaleGradient extends Component {

    renderIcon() {
        const {icon, iconSize, iconStyle} = this.props;
        if (icon) {
            return (
                <SimpleLineIcons
                    size={iconSize || 40}
                    name={icon}
                    style={{...styles.icon, ...iconStyle}}
                />
            );
        }
        return null;
    }

    renderTitle() {
        const {title, titleStyle} = this.props;
        if (title) {
            return (
                <HamraaText style={{...styles.title, ...titleStyle}}>
                    {title}
                </HamraaText>
            );
        }
        return null;
    }

    renderDescription() {
        const {description, descriptionStyle} = this.props;
        if (description) {
            return (
                <HamraaText style={{...styles.description, ...descriptionStyle}}>
                    {description}
                </HamraaText>
            );
        }
        return null;
    }

    render() {
        const {
            children, onPress, gradientColors, containerStyle, linearGradientStyle, ...otherProps
        } = this.props;
        return (
            <TouchableScale
                friction={90}
                tension={100}
                onPress={onPress}
                style={{...styles.container, ...containerStyle}}
                activeScale={0.95}
                {...otherProps}
            >
                <LinearGradient
                    start={{x: 1, y: 0}}
                    end={{x: 0.2, y: 0}}
                    colors={gradientColors || Colors.primaryGradient}
                    style={{...styles.linearGradient, ...linearGradientStyle}}
                >
                    {this.renderIcon()}
                    {this.renderTitle()}
                    {this.renderDescription()}
                    {children}
                </LinearGradient>
            </TouchableScale>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        marginBottom: 16,
        color: Colors.white
    },
    title: {
        fontWeight: 'normal',
        fontFamily: 'IranYekan',
        color: Colors.primaryTextLight,
        fontSize: Dimensions.fontMediumXLarge
    },
    description: {
        fontWeight: 'normal',
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMedium,
        color: Colors.primaryTextLight,
    },
});

export {HamraaScaleGradient} ;
