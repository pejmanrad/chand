import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../../values';
import {HamraaText} from '.';

class HamraaSectionTitled extends Component {
    render() {
        const {children, title, action, onActionPress} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.containerHeader}>
                    <HamraaText style={styles.title}>{title}</HamraaText>
                    <HamraaText clickable style={styles.action} onPress={onActionPress}>{action}</HamraaText>
                </View>
                {children}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerHeader: {
        flex: 1,
        alignItems: 'center',
        marginHorizontal: 36,
        flexDirection: 'row-reverse',
        justifyContent: 'space-between'
    },
    title: {
        fontFamily: 'IranYekanBold',
    },
    action: {
        color: Colors.primary,
    }
});

export {HamraaSectionTitled};