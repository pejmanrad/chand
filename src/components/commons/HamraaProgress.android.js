import React, {Component} from 'react';
import {StyleSheet, ProgressBarAndroid} from 'react-native';
import {Colors} from '../../values';

class HamraaProgress extends Component {
    render() {
        const {
            progress, indeterminate, ...otherProps
        } = this.props;
        return (
            <ProgressBarAndroid
                progress={progress}
                styleAttr="Horizontal"
                color={Colors.primary}
                indeterminate={indeterminate || false}
                style={styles.container}
                {...otherProps}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 4,
        marginHorizontal: 4,
        transform: [{ rotate: '180deg'}]
    },
});

export {HamraaProgress} ;