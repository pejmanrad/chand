import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Header} from 'react-native-elements';
import {Colors, Dimensions} from '../../values';

class HamraaHeader extends Component {
    render() {
        const {title, iconLeft, iconLeftOnPress, iconRight, iconRightOnPress, textStyle, containerStyle} = this.props;
        return (
            <Header
                leftComponent={{icon: iconLeft, color: Colors.primaryDark, onPress: iconLeftOnPress}}
                centerComponent={{text: title, style: [styles.text, textStyle]}}
                rightComponent={{icon: iconRight, color: Colors.primaryDark, onPress: iconRightOnPress}}
                containerStyle={{...styles.container, ...containerStyle}}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 45,
        paddingTop: 0,
        backgroundColor: Colors.primaryLight
    },
    text: {
        color: Colors.primaryDark,
        fontSize: Dimensions.fontLarge,
        fontFamily: 'IranYekan'
    },
});

export {HamraaHeader} ;