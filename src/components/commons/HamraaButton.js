import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, Dimensions} from '../../values';

class HamraaButton extends Component {
    render() {
        const {
            loading, title, icon, iconRight,
            containerStyle, buttonStyle, titleStyle,
            onPress, ...otherProps
        } = this.props;
        return (
            <Button
                loading={loading}
                title={title}
                icon={icon ? (<Icon name={icon} color={Colors.white} size={18}/>) : null}
                iconRight={iconRight}
                containerStyle={{...styles.container, ...containerStyle}}
                buttonStyle={{...styles.button, ...buttonStyle}}
                titleStyle={{...styles.title, ...titleStyle}}
                onPress={onPress}
                disabled={loading}
                raised
                {...otherProps}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: -1,
        marginVertical: 10,
    },
    button: {
        // width: 250,
        height: 45,
        borderRadius: 22.5,
        paddingHorizontal: 24,
        backgroundColor: Colors.yellowBotton,
    },
    title: {
        fontFamily: 'IranYekan',
        fontSize: Dimensions.fontMediumXLarge,
        color: Colors.primaryTextDark
    },
});

export {HamraaButton} ;
