import React, {Component} from 'react';
import {StyleSheet, ProgressViewIOS} from 'react-native';
import {Colors} from '../../values';

class HamraaProgress extends Component {
    render() {
        const {
            progress, ...otherProps
        } = this.props;
        return (
            <ProgressViewIOS
                progress={progress}
                progressTintColor={Colors.primary}
                style={styles.container}
                {...otherProps}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 4,
        marginHorizontal: 4,
        transform: [{rotate: '180deg'}]
    },
});

export {HamraaProgress} ;