import React, {Component} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {FlatList, PixelRatio, StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import {Colors, Dimensions} from '../../values';

class HamraaSimpleList extends Component {

    renderItem = ({item, index}) => {
        return (
            <TouchableHighlight
                underlayColor="#dddddd"
                style={styles.rowTouchable}
                onPress={() => this.props.onItemPress(item, index)}>
                <View style={styles.row}>
                    <Text style={styles.rowLeft}>
                        <MaterialCommunityIcons name="chevron-left" size={18} color="#aaaaaa"/>
                    </Text>
                    <Text style={styles.rowLabel}>{item.title}</Text>
                    <Text style={styles.rowIcon}>
                        <MaterialCommunityIcons name={item.icon} size={24} color={Colors.primaryTextDarkSub}/>
                    </Text>
                </View>
            </TouchableHighlight>
        );
    };

    keyExtractor = item => item.id;

    scrollToTop = () => {
        this.listView.scrollTo({x: 0, y: 0});
    };

    render() {
        return (
            <FlatList
                ref={view => {
                    this.listView = view;
                }}
                stickySectionHeadersEnabled
                removeClippedSubviews={false}
                keyboardShouldPersistTaps="handled"
                keyboardDismissMode="on-drag"
                contentContainerStyle={{backgroundColor: '#fff'}}
                data={this.props.data}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    rowLeft: {
        alignSelf: 'flex-start',
        paddingHorizontal: 4,
    },
    rowRight: {
        alignSelf: 'flex-end',
        paddingHorizontal: 4,
    },
    rowTouchable: {
        paddingHorizontal: 10,
        paddingVertical: 16,
        borderBottomWidth: 1.0 / PixelRatio.get(),
        borderBottomColor: '#dddddd',
    },
    rowLabel: {
        flex: 1,
        color: Colors.primaryTextDark,
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMediumLarge,
    },
    rowIcon: {
        alignSelf: 'flex-end',
        marginRight: 6,
        marginLeft: 10,
    },
});

export default HamraaSimpleList;