import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {StyleSheet, View,} from 'react-native';
import {Divider, ListItem} from 'react-native-elements';
import suggest from './services/suggest';
import {HamraaTextInput} from '../HamraaTextInput';
import {Colors, Dimensions} from '../../../values';

class HamraaSuggesterInput extends Component {

    constructor(props) {
        super(props);
        this.state = {data: [], value: ''};
    }

    onPressItem = (id: string, name: string) => {
        // updater functions are preferred for transactional updates
        const {onDataSelectedChange} = this.props;
        const existingItem = {id, name};
        this.setState({
            value: name,
        });
        onDataSelectedChange(existingItem);
    };

    searchList = async (text) => {
        const {
            keyPathRequestResult,
            itemFormat,
            apiEndpointSuggestData,
            onDataSelectedChange,
            staticData,
        } = this.props;
        this.setState({value: text});
        let suggestData = null;
        if (staticData != null) {
            try {
                suggestData = suggest.searchForRelevant(text, staticData, itemFormat);
            } catch (e) {
                suggestData = {suggest: [], existingItem: null};
            }
        } else {
            try {
                suggestData = await suggest.searchForSuggest(
                    text,
                    apiEndpointSuggestData,
                    keyPathRequestResult,
                    itemFormat,
                );
            } catch (e) {
                suggestData = {suggest: [], existingItem: null};
            }
        }
        onDataSelectedChange(suggestData.existingItem);
        this.setState({
            data: suggestData.suggest,
        });
    };

    renderList = () => {
        const {data} = this.state;
        if (data.length > 0) {
            const {listStyle, itemTextStyle, itemTagStyle} = this.props;
            return (
                <View style={[styles.list, listStyle]}>
                    <Divider style={{backgroundColor: Colors.primaryLight, marginHorizontal: 10}}/>
                    {data.map((item, position) =>
                        <ListItem
                            key={item.id}
                            title={item.tags[0].item}
                            rightTitle={item.name}
                            underlayColor="#dddddd"
                            style={{
                                ...styles.listItem,
                                ...(position === data.length - 1 ? {
                                    borderBottomLeftRadius: 23,
                                    borderBottomRightRadius: 23,
                                } : {})
                            }}
                            onPress={() => this.onPressItem(item.id, item.name)}
                            // Component={TouchableOpacity}
                            containerStyle={{
                                ...styles.listItemContainer,
                                ...(position === data.length - 1 ? {
                                    paddingBottom: 8,
                                    borderBottomLeftRadius: 23,
                                    borderBottomRightRadius: 23,
                                } : {})
                            }}
                            titleStyle={{...styles.itemDescription, ...itemTagStyle}}
                            rightTitleStyle={{...styles.itemTitle, ...itemTextStyle}}
                        />
                    )}
                </View>
            );
        }
    };

    render() {
        const {value, data} = this.state;
        const {containerStyle, inputStyle, ...otherProps} = this.props;
        return (
            <View
                style={[styles.container, containerStyle]}
                keyboardShouldPersist="always"
            >
                <HamraaTextInput
                    value={value}
                    clearButtonMode="while-editing"
                    onChangeText={this.searchList}
                    style={{marginBottom: 0}}
                    containerStyle={{
                        ...styles.input,
                        ...inputStyle, ...(data.length > 0 ? {
                            borderBottomWidth: 0,
                            borderBottomLeftRadius: 0,
                            borderBottomRightRadius: 0,
                        } : {})
                    }}
                    {...otherProps}
                />
                {this.renderList()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    input: {
        borderWidth: 2,
        borderRadius: 23,
        borderColor: Colors.primaryTextLightSub,
        backgroundColor: Colors.white,
    },
    list: {
        flex: 1,
        // alignSelf: 'center',
        // width: Dimensions.window.width - 20,
        //// height: '100%',
        ////// position: 'absolute',
        ////// top: 50,
        ////// left: 0,
        ////// right: 0,
        marginHorizontal: 10,
        borderBottomLeftRadius: 23,
        borderBottomRightRadius: 23,
        borderWidth: 2,
        borderTopWidth: 0,
        borderColor: Colors.primaryTextLightSub,
    },
    listItem: {},
    listItemContainer: {
        // height: 36,
        paddingVertical: 6,
    },
    itemTitle: {
        textAlign: 'right',
        fontWeight: 'normal',
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMediumXLarge,
        color: Colors.primaryTextDark,
    },
    itemDescription: {
        textAlign: 'left',
        fontWeight: 'normal',
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMediumXLarge,
        color: Colors.primaryTextDarkSub,
    },
});

HamraaSuggesterInput.propTypes = {
    inputStyle: PropTypes.shape({}),
    listStyle: PropTypes.shape({}),
    containerStyle: PropTypes.shape({}),
    itemTextStyle: PropTypes.shape({}),
    itemTagStyle: PropTypes.shape({}),
    apiEndpointSuggestData: PropTypes.func,
    staticData: PropTypes.arrayOf(PropTypes.shape({})),
    onDataSelectedChange: PropTypes.func,
    keyPathRequestResult: PropTypes.string,
    itemFormat: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        tags: PropTypes.arrayOf(PropTypes.string),
    }),
};
HamraaSuggesterInput.defaultProps = {
    inputStyle: {},
    listStyle: {},
    containerStyle: {},
    itemTextStyle: {},
    itemTagStyle: {},
    staticData: null,
    apiEndpointSuggestData: () => _.noop,
    onDataSelectedChange: () => _.noop,
    keyPathRequestResult: 'suggest.city[0].options',
    itemFormat: {
        id: 'id',
        name: 'name',
        tags: [],
    },
};

export {HamraaSuggesterInput};
