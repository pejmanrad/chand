import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Input} from 'react-native-elements';
import {Colors, Dimensions} from '../../values';

class HamraaTextInputLinear extends Component {
	render() {
		const {icon, inputRef, style, containerStyle,iconSize,
			...otherProps} = this.props;
		return (
			<View style={[styles.container, style]}>
				<Input
					{...otherProps}
					ref={inputRef}
					inputContainerStyle={{...styles.inputContainer,
						...containerStyle}}
					leftIcon={<Icon name={icon} color={Colors.white} size={iconSize}/>}
					leftIconContainerStyle={{paddingHorizontal: 5}}
					inputStyle={styles.input}
					labelStyle={styles.label}
					autoFocus={false}
					autoCapitalize="none"
					keyboardAppearance="light"
					errorStyle={styles.inputError}
					autoCorrect={false}
					blurOnSubmit={false}
					
					placeholderTextColor={Colors.white}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: 'stretch',
		marginVertical: 10,
	},
	inputContainer: {
		paddingHorizontal: 10,
		borderBottomWidth: 1,
		borderColor: Colors.primaryLight,
		 backgroundColor: Colors.transparent,
		borderRadius:30,
		height: 45,
	},
	input: {
		flex: 1,
		marginLeft: 10,
		textAlign: 'right',
		fontFamily: 'IranYekanLight',
		fontSize: Dimensions.fontMediumXLarge,
		color: Colors.white
	},
	inputError: {
		marginTop: 0,
		textAlign: 'center',
		color: Colors.white,
		fontFamily: 'IranYekanLight',
		fontSize: Dimensions.fontMedium,
	},
	label: {
		color: Colors.white,
		fontWeight: 'bold',
		fontFamily: 'IranYekanLight',
		fontSize: Dimensions.fontMedium,
		marginHorizontal: 24,
	
	},
});

export {HamraaTextInputLinear} ;
