import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors, Dimensions} from '../../values';
import {HamraaScale} from './HamraaScale';

class HamraaSummary extends Component {
    render() {
        const {
            icon,
            mainTitle,
            mainDescription,
            firstTitle,
            firstDescription,
            secondTitle,
            secondDescription,
            thirdTitle,
            thirdDescription,
            mainKey,
            firstKey,
            secondKey,
            thirdKey,
            onPress,
            mainScaleStyle,
            secondaryScaleStyle
        } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.containerLeft}>
                    <HamraaScale
                        title={firstTitle}
                        description={firstDescription}
                        onPress={() => onPress(firstKey)}
                        containerStyle={{borderTopLeftRadius: 24, height: 64, ...secondaryScaleStyle}}
                    />
                    <HamraaScale
                        title={secondTitle}
                        description={secondDescription}
                        onPress={() => onPress(secondKey)}
                        containerStyle={{height: 64, ...secondaryScaleStyle}}
                    />
                    <HamraaScale
                        title={thirdTitle}
                        description={thirdDescription}
                        onPress={() => onPress(thirdKey)}
                        containerStyle={{borderBottomLeftRadius: 24, height: 64, ...secondaryScaleStyle}}
                    />
                </View>
                <View style={styles.containerRight}>
                    <HamraaScale
                        icon={icon}
                        title={mainTitle}
                        description={mainDescription}
                        titleStyle={styles.title}
                        onPress={() => onPress(mainKey)}
                        descriptionStyle={styles.description}
                        containerStyle={{...styles.scale, ...mainScaleStyle}}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginLeft: 16,
        marginRight: 16,
        marginTop: 8,
        marginBottom: 8,
        flexDirection: 'row',
        minHeight: 180,
        borderRadius: 24,
    },
    containerLeft: {
        flex: 0.35,
        borderRightWidth: 1,
        borderRightColor: Colors.white,
    },
    containerRight: {
        flex: 0.65,
    },
    scale: {
        borderTopRightRadius: 24,
        borderBottomRightRadius: 24,
    },
    title: {
        fontFamily: 'IranYekanBold',
        fontSize: Dimensions.fontMediumXXLarge
    },
    description: {
        color: Colors.primaryTextLightSub,
    }
});

export {HamraaSummary};