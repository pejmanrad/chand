import React, {Component} from 'react';
import {Avatar} from 'react-native-elements';
import {StyleSheet} from 'react-native';
import {Dimensions} from '../../values';
import {Colors} from '../../values/Colors';

class HamraaAvatar extends Component {

    render() {

        const {
            size,
            title,
            source,
            onPress,
            rounded,
            placeholder,
            showEditButton,
            titleStyle,
            avatarStyle,
            containerStyle,
            placeholderStyle,
            overlayContainerStyle,
            ...otherProps
        } = this.props;

        return (
            <Avatar
                size={size}
                title={title}
                source={source}
                onPress={onPress}
                rounded={rounded}
                renderPlaceholderContent={placeholder}
                showEditButton={showEditButton}
                titleStyle={{...styles.title, ...titleStyle}}
                avatarStyle={{...styles.avatar, ...avatarStyle}}
                placeholderStyle={{...styles.placeholder, ...placeholderStyle}}
                overlayContainerStyle={{...styles.overlayContainer, ...overlayContainerStyle}}
                containerStyle={{...styles.container, ...containerStyle}}
                {...otherProps}
            />
        );
    }
}

const styles = StyleSheet.create({

    container: {},

    overlayContainer: {},

    avatar: {
        // borderWidth: 2,
        // borderRadius: 64,
        // borderColor: Colors.white,
    },

    title: {
        fontWeight: 'normal',
        fontFamily: 'IranYekanBold',
        fontSize: Dimensions.fontMedium,
    },

    placeholder: {
        backgroundColor: Colors.white
    },

});

export {HamraaAvatar};