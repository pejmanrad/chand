import React from 'react'
import { FlatList,ActivityIndicator} from 'react-native';
class PRList extends React.Component{
	render(){
		const {data,listFooter,endReached,threshold,renderItem,...otherProps} =this.props;
		return(<FlatList
			data={data}
			keyExtractor={(item, index) => index.toString()}
			ListFooterComponent={listFooter?<ActivityIndicator size={'large'} />:null}
			showsVerticalScrollIndicator={false}
			showsHorizontalScrollIndicator={false}
			contentContainerStyle={{paddingVertical:10}}
			onEndReached={endReached}
			onEndReachedThreshold={threshold}
			renderItem={renderItem}
			{...otherProps}
	/>)
	}
}
export {PRList}
