import LinearGradient from 'react-native-linear-gradient';
import {Colors} from '../../values';
import {Text, View, StyleSheet} from 'react-native';
import React, {Component} from 'react';

class HeaderGradiant extends Component {
	render() {
		const {title, leftIcon, rightIcon,style} = this.props;
		
		return (
			<View >
				<LinearGradient
					colors={Colors.primaryGradient}
					style={[styles.GradiantStyle,style]}
				>
					<View style={styles.Main}>
						<View style={styles.topLine}>
							<View>
								{leftIcon ? leftIcon : null}
							</View>
							<View>
								{rightIcon ? rightIcon : null}
							</View>
						</View>
						{title ? <View style={styles.HeaderText}><Text style={styles.title}>{title}</Text></View> : null}
					</View>
				</LinearGradient>
			</View>
		);
		
		
	}
}

const styles = StyleSheet.create({
	GradiantStyle: {
		borderBottomRightRadius: 30,
		borderBottomLeftRadius: 30,
	},
	Main: {
		paddingTop: 5,
		flexDirection: 'column',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '100%',
		height: 70,
		borderBottomRightRadius: 30,
		borderBottomLeftRadius: 30,
		backgroundColor: 'transparent',
	},
	topLine: {
		flexDirection: 'row',
		width: '95%',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	HeaderText: {justifyContent: 'center', alignItems: 'center', paddingBottom: 5},
	title: {
		color: '#fff',
	},
});
export {HeaderGradiant};
