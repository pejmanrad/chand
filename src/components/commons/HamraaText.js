import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Colors, Dimensions} from '../../values';

type Props = {
    clickable?: boolean,
};

class HamraaText extends Component<Props> {

    renderClickable() {
        return (
            <TouchableOpacity onPress={this.props.onPress}
             style={styles.container}>
                <Text style={[styles.text, 
                    {color: Colors.primaryDark}, 
                    this.props.style]}>
                    {this.props.children}
                    </Text>
            </TouchableOpacity>
        );
    }

    render() {

        const {clickable, onPress, children, style, ...otherProps} = this.props;

        if (this.props.clickable) {
            return this.renderClickable();
        }

        return (
            <Text onPress={onPress} style={[styles.text, style]} {...otherProps}>
                {children}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 45,
    },
    text: {
        color: Colors.primaryTextDark,
        fontSize: Dimensions.fontMediumXLarge,
        fontFamily: 'IranYekanLight'
    },
});

export {HamraaText} ;