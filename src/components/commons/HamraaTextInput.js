import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Input} from 'react-native-elements';
import {Colors, Dimensions} from '../../values';

class HamraaTextInput extends Component {
    render() {
        const {icon, inputRef, style, containerStyle,iconSize,muluti,
             ...otherProps} = this.props;
        return (
            <View style={[styles.container, style]}>
                <Input
                    {...otherProps}
                    ref={inputRef}
                    inputContainerStyle={{...styles.inputContainer,
                         ...containerStyle}}
                    rightIcon={<Icon name={icon} color={Colors.primaryDark} size={18}/>}
                    rightIconContainerStyle={{paddingHorizontal: 5,}}
                    inputStyle={styles.input}
                    labelStyle={styles.label}
                    autoFocus={false}
                    autoCapitalize="none"
                    multiline={muluti}
                    keyboardAppearance="light"
                    errorStyle={styles.inputError}
                    autoCorrect={false}
                    blurOnSubmit={false}
                    placeholderTextColor={Colors.primaryTextDarkSub}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignSelf: 'stretch',
        marginVertical: 10,
    },
    inputContainer: {
        paddingHorizontal: 10,
        borderRadius: 40,
        borderWidth: 1,
        borderColor: Colors.primaryLight,
        backgroundColor: Colors.primaryLight,
        height: 45,
    },
    input: {
        flex: 1,
        marginLeft: 10,
        textAlign: 'right',
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMediumXLarge,
        color: Colors.primaryTextDark
    },
    inputError: {
        marginTop: 0,
        textAlign: 'center',
        color: Colors.red,
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMedium,
    },
    label: {
        color: Colors.primaryTextDark,
        fontWeight: 'normal',
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMedium,
        marginHorizontal: 24
    },
});

export {HamraaTextInput} ;
