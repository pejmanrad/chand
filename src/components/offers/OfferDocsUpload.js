import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View, Platform} from 'react-native';
import {Icon, ListItem} from 'react-native-elements';
// noinspection ES6CheckImport
import {DocumentPicker, DocumentPickerUtil,} from 'react-native-document-picker';
import {HamraaProgress} from '../commons';
import {Colors, Strings} from '../../values';
import {offerSet} from '../../actions';

class OfferDocsUpload extends Component {

    showDocumentPicker = () => {
        DocumentPicker.show({
            filetype: [DocumentPickerUtil.images()],
        }, (error, res) => {
            Platform.select({
                ios: () => {
                },
                android: () => {
                    if (res !== null) {
                        this.props.offerSet({requestFormFile: {...res}});
                    }
                },
            })();
        });
    };

    render() {
        const {
            offerSet,
            offerType,
            requestFormFile,
            requestFormUploading,
            requestFormUploadingProgress,
        } = this.props;
        return (
            <View style={styles.container}>
                <ListItem
                    title={
                        requestFormFile ? requestFormFile.fileName : Strings.titleRequestForm
                    }
                    subtitle={
                        <HamraaProgress progress={requestFormUploadingProgress}/>
                    }
                    leftIcon={
                        requestFormUploading === true ?
                            {
                                type: 'material-community',
                                name: 'close',
                                color: Colors.primaryTextDarkSub
                            }
                            : requestFormUploading === false ?
                            {
                                type: 'material-community',
                                name: 'check',
                                color: Colors.primary
                            }
                            : null
                    }
                    rightIcon={
                        <Icon
                            raised
                            reverse
                            name='file-document-outline'
                            type='material-community'
                            onPress={this.showDocumentPicker}
                            color={Colors.primaryActive}
                        />
                    }
                    titleStyle={styles.title}
                    containerStyle={styles.item}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 24,
    },
    item: {},
    title: {
        textAlign: 'right',
        fontWeight: 'normal',
        fontFamily: 'IranYekanLight',
        color: Colors.primaryTextDark,
    },
});

const mapStateToProps = ({offer}) => {
    return {...offer};
};

export default connect(
    mapStateToProps,
    {offerSet}
)(OfferDocsUpload);