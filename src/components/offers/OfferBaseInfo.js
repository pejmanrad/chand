import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View} from 'react-native';
import {HamraaTextInput} from '../commons';
import {Strings} from '../../values';
import {offerSet} from '../../actions';

class OfferBaseInfo extends Component {

    render() {
        const {
            description,
            descriptionValid,
            offerPrice,
            offerPriceValid,
            offerSet,
        } = this.props;
        return (
            <View style={styles.container}>
                <HamraaTextInput
                    inputRef={input => this.descriptionInput = input}
                    icon="book-open"
                    label={Strings.titleDescription}
                    value={description}
                    onChangeText={description => offerSet({description})}
                    placeholder={Strings.hintDescription}
                    keyboardType="default"
                    returnKeyType="next"
                    multiline={true}
                    containerStyle={{height: undefined,}}
                    errorMessage={
                        descriptionValid ? null : Strings.errorInvalidDescription
                    }
                    onSubmitEditing={() => {

                    }}
                />
                <HamraaTextInput
                    inputRef={input => this.offerPriceInput = input}
                    icon="wallet"
                    label={Strings.titleOfferPrice}
                    value={offerPrice}
                    onChangeText={offerPrice => offerSet({offerPrice})}
                    placeholder={Strings.hintPrice}
                    returnKeyType="next"
                    errorMessage={
                        offerPriceValid ? null : Strings.errorInvalidPrice
                    }
                    onSubmitEditing={() => {

                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        paddingHorizontal: 24,
    },

});


const mapStateToProps = ({offer}) => {
    return {...offer};
};

export default connect(
    mapStateToProps,
    {offerSet}
)(OfferBaseInfo);