import React, {Component} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import OfferItem from './OfferItem';
import {HamraaScale} from '../commons';
import OfferItemHorizental from './OfferItemHorizental';

type Props = {
    data: Array,
    onItemPress: function,
    vertical: boolean,
    inverted: boolean,
};

class OfferList extends Component<Props> {
    static defaultProps = {
        vertical: true,
        inverted: false,
    };

    render() {
        const {data, vertical, inverted, onItemPress, containerStyle} = this.props;
        return (
            <View style={[styles.container, containerStyle]}>
                <FlatList
                    data={data}
                    keyExtractor={(item) => String(item.id)}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={vertical ? styles.vertical : styles.horizontal}
                    horizontal={!vertical}
                    inverted={inverted}
                    renderItem={
                        ({item, index}) => {
                            return (
                                <HamraaScale onPress={() => onItemPress(item, index)}>
                                    {vertical ? <OfferItem offer={item} index={index}/> :
                                        <OfferItemHorizental offer={item} index={index}/>}
                                </HamraaScale>
                            );
                        }
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    vertical: {
        paddingVertical: 8
    },
    horizontal: {
        paddingHorizontal: 8
    },
});

export default OfferList;