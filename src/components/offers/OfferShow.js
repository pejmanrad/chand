import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {HeaderGradiant} from '../commons';
import {Icon} from 'react-native-elements';
import {Colors} from '../../values';
import {orderCreate} from '../../actions';

class OfferShow extends React.Component {
	
	createOrder = (offer) => {
		this.props.orderCreate({offerId: offer.id, requestId: offer.requestId});
	};
	
	render() {
		const {offer} = this.props;
		return (<View style={styles.container}>
			<HeaderGradiant
				title='لیست درخواست'
				rightIcon={<Icon name='menu' type='MaterialIcons' color='#fff' size={26}/>}
				leftIcon={<Icon name='chevron-left' type='MaterialIcons' color='#fff' size={26}
									 onPress={() => this.props.navigation.goBack()}/>}
			/>
			<View style={{padding: 10}}>
				<Text style={styles.text}>{offer.title}</Text>
				<Text style={styles.text}>{offer.price}</Text>
				<Image source={require('../../../assets/images/question.png')} style={styles.image}/>
				
				<TouchableOpacity style={styles.touchable} onPress={() => this.createOrder(offer)}>
					<Text style={{color: 'white', fontSize: 15}}>ثبت سفارش</Text>
				</TouchableOpacity>
			</View>
		</View>);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	touchable: {
		backgroundColor: Colors.primary,
		padding: 10,
		borderRadius: 5,
		justifyContent: 'center',
		alignItems: 'center',
	},
	text: {
		padding: 10, marginTop: 10,
		borderRadius: 5,
		backgroundColor: '#f8f8f8',
		textAlign: 'right',
	},
	image: {
		borderRadius: 5, margin: 10,
		alignSelf: 'center',
		maxHeight: 300,
	},
});
const mapStateToProps = ({offer}) => {
	return {...offer};
};
export default connect(mapStateToProps, {orderCreate})(OfferShow);
