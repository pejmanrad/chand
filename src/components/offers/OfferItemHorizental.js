import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors, Dimensions, Strings} from '../../values';
import {HamraaText} from '../commons';

class OfferItemHorizental extends Component {
    render() {
        const {offer, index} = this.props;
        // let firstItemStyle = {};
        // if (index === 0) {
        //     firstItemStyle.marginRight = 16;
        // }
        return (
            <View style={{...styles.container}}>
                <View>
                    <HamraaText
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={styles.title}>{offer.description}
                    </HamraaText>
                </View>
                <View style={{alignItems: 'center', paddingHorizontal: 12, paddingVertical: 6}}>
                    <HamraaText style={styles.description}>{Strings.titleOfferPrice}</HamraaText>
                    <HamraaText style={{...styles.description, color: Colors.primaryTextDark,}}>
                        {`${offer.price} تومان `}
                    </HamraaText>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 100,
        width: 220,
        marginVertical: 4,
        marginHorizontal: 8,
        borderRadius: 24,
        justifyContent: 'space-between',
        backgroundColor: Colors.primaryLight
    },
    title: {
        padding: 6,
        fontFamily: 'IranYekan',
        textAlign: 'center',
        color: Colors.primaryTextLight,
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        backgroundColor: 'rgba(0, 0, 0, 0.24)',
    },
    description: {
        fontFamily: 'IranYekanLight',
        fontSize: Dimensions.fontMedium,
        color: Colors.primaryTextDarkSub,
    }
});

export default OfferItemHorizental;