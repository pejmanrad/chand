import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withNavigation} from 'react-navigation';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import RequestList from '../requests/RequestList';
import {HamraaTextInput} from '../commons';
import {requestAll} from '../../actions';
import {Strings} from '../../values';

class VendorDashboard extends Component {

    componentDidMount() {
        this.props.requestAll();
    }

    handleItemPress = (item, index) => {
        this.props.navigation.navigate('RequestDetails', {item, index});
    };

    renderSearchBar = () => {
        return (
            <HamraaTextInput
                inputRef={input => (this.nameInput = input)}
                icon="magnifier"
                // value={name}
                // onChangeText={name => authSet({name})}
                placeholder={Strings.hintSearch}
                keyboardType="default"
                returnKeyType="next"
                // style={{marginTop: 0}}
            />
        );
    };

    renderLoading = () => {
        if (this.props.loading) {
            return (
                <View style={{marginVertical: 8}}>
                    <ActivityIndicator/>
                </View>
            );
        } else {
            return (
                <View>
                </View>
            );
        }
    };

    render() {
        const {all} = this.props;
        return (
            <View style={styles.container}>
                <RequestList
                    data={all}
                    ListFooterComponent={this.renderLoading}
                    ListHeaderComponent={this.renderSearchBar}
                    onItemPress={this.handleItemPress}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

});


const mapStateToProps = ({request}) => {
    return {...request};
};

export default connect(
    mapStateToProps,
    {requestAll}
)(withNavigation(VendorDashboard));