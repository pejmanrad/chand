import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import AppSwitch from './navigators/AppSwitch';

const AppContainer = createAppContainer(
    createSwitchNavigator(
        {
            Index: AppSwitch
        },
        {
            initialRouteName: 'Index',
            headerMode: 'none',
        }
    )
);

export default AppContainer;