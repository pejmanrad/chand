import AsyncStorage from '@react-native-community/async-storage';
import SInfo from 'react-native-sensitive-info';
import chand from '../apis/chand';
import FS from 'react-native-fs';
import NavigationService from '../NavigationService';
import { Strings } from '../values';
import { ActionTypes } from './types';
import { Platform } from 'react-native';

export const authSet = (values) => {
	return {
		type: ActionTypes.AUTH_SET,
		payload: values,
	};
};
export const userSet = (values) => {
	console.log(values)
	return {
		 type: ActionTypes.USER_EDIT,
		  payload: values
		 }
}

export const generalSet = (values) => {
	return {
		type: ActionTypes.GENERAL_SET,
		payload: values,
	};
};
export const Base = () => async (dispatch) => {
	try {
		const response = await chand.post('/generals/base', {
			os: Platform.Os === 'ios' ? 'ios' : 'android',
			version: Strings.appVersion,
		});

		handleResponse(response,
			async (data) => {
				dispatch(generalSet({ categories: data.categories, appInfo: data.appInfo }));
			},
			(errorMessage) => {
				console.log(errorMessage);
				// dispatch({type: ActionTypes.AUTH_SIGN_IN_CONFIRM, payload: {error: errorMessage, ...values}});
			},
		);
	} catch (e) {

	}
};

export const categorySet = (values) => {
	return {
		type: ActionTypes.CATEGORY_SET,
		payload: values,
	};
};

export const categoryReset = () => {
	return {
		type: ActionTypes.CATEGORY_RESET,
	};
};
export const signIn = (values) => async (dispatch) => {

	dispatch(authSet({ loading: true, error: null }));
	try {
		const response = await chand.post('/checkin', { ...values });
		handleResponse(response,
			async (data) => {
				dispatch({
					type: ActionTypes.AUTH_SIGN_IN_CONFIRM,
					payload: {
						// token: data.token,
						type: data.type,
						// legal: data.is_legal === 1,
					},
				});
				NavigationService.navigate('SingInConfirm');
				dispatch(authSet({ loading: false }));

			},
			(errorMessage) => {

				dispatch({ type: ActionTypes.AUTH_SIGN_IN, payload: { error: errorMessage, ...values } });
			},
		);
	} catch (e) {
		console.log({ e });
		handleError(e, (errorMessage) => {
			dispatch(authSet({ loading: false, error: errorMessage }));
		});
	}
};

export const signInConfirm = (values) => async (dispatch) => {
	dispatch(authSet({ loading: true, error: null }));
	try {
		const response = await chand.post('/verify', { ...values });

		handleResponse(response,
			async (data) => {
				SInfo.setItem('token', data.accessToken, {});

				dispatch({

					type: ActionTypes.AUTH_SIGN_IN_CONFIRM,
					payload: {
						token: data.accessToken,
					},
				});
				NavigationService.navigate('Main');
			},
			(errorMessage) => {
				console.log(errorMessage);
				dispatch({ type: ActionTypes.AUTH_SIGN_IN_CONFIRM, payload: { error: errorMessage, ...values } });
			},
		);
	} catch (e) {
		dispatch(authSet({ loading: false, error: e.response.data.errors.general }));
		// handleError(e, (errorMessage) => {
		//     dispatch(authSet({loading: false, error: errorMessage}));
		// });
	}
};
export const signUp = (values) => async (dispatch) => {

	dispatch(authSet({ loading: true, error: null }));

	try {
		const response = await chand.post('/register', { ...values });
		handleResponse(response,
			async (data) => {
				await AsyncStorage.multiSet([
					['token', data.token],
					['name', values.name],
					['email', values.email],
				]);
				dispatch({
					type: ActionTypes.AUTH_SIGN_UP,
					payload: {
						token: data.token,
						name: values.name,
						email: values.email,
						type: values.type,
					},
				});
				// NavigationService.navigate('CompanyRegister');
				NavigationService.navigate('Main');
			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.AUTH_SIGN_UP, payload: { error: errorMessage, ...values } });
			},
		);
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(authSet({ loading: false, error: errorMessage }));
		});
	}
};

export const signOut = () => {
	return {
		type: ActionTypes.AUTH_SIGN_OUT,
	};
};


export const requestSet = (values) => {
	return {
		type: ActionTypes.TENDER_SET,
		payload: values,
	};
};

export const requestAll = () => async (dispatch, getState) => {
	dispatch(requestSet({ loading: true, error: null }));
	try {
		const response = await chand.get('/requests');
		handleResponse(response,
			(data) => {
				const requests = data.requests;
				dispatch({
					type: ActionTypes.TENDER_ALL,
					payload: {
						all: requests,
						pagination: data.pagination,
					},
				});
			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.TENDER_ALL, payload: { error: errorMessage } });
			},
		);
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(requestSet({ loading: false, error: errorMessage + e }));
		});
	}
};
export const requestMoreData = ({ reqPage }) => async (dispatch, getState) => {
	dispatch(requestSet({ fetchLoading: true }));
	try {
		const response = await chand.get('/requests', {
			params: {
				page: reqPage,
			},
		});
		handleResponse(response,
			(data) => {
				const requests = data.requests;
				const pagination = data.pagination;

				dispatch({
					type: ActionTypes.TENDER_MORE, payload: {
						all: [...getState().request.all.slice(0, reqPage * pagination.size), ...requests],
						fetchLoading: false,
						pagination: data.pagination,
						page: reqPage,
					},
				});

			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.TENDER_ALL, payload: { error: errorMessage } });
			});
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(requestSet({ loading: false, error: errorMessage + e }));
		});
	}
};



export const requestCreate = values => async (dispatch, getState) => {
	dispatch(requestSet({ loading: true, error: null }));

	try {
		let data = new FormData();
		data.append('title', values.title)
		data.append('categoryId', values.categoryId)
		data.append('description', values.description)
		if (values.image) {
			let img = values.image.path.split('/')
			data.append('image', { uri: values.image.path, name: img[img.length - 1], type: values.image.mime })
		}
		const response = await chand.post('/requests', data, {
			headers: {
				'Content-Type': 'multipart/form-data'
			}
		});
		console.log(response)
		handleResponse(response,
			(data) => {
				dispatch(requestSet({
					title: '',
					description: '',
					requestFormFile: null,
				}));
				dispatch(requestAll());
				NavigationService.navigate('Requests');
			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.TENDER_CREATE, payload: { error: errorMessage, ...values } });
			},
		);
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(requestSet({ loading: false, error: errorMessage }));
		});
	}
};

export const requestReset = () => {
	return {
		type: ActionTypes.TENDER_RESET,
	};
};


export const offerSet = (values) => {
	return {
		type: ActionTypes.BID_SET,
		payload: values,
	};
};

export const offerAll = (values) => async (dispatch, getstate) => {
	dispatch(offerSet({ loading: true, error: null, all: [] }));
	try {
		console.log(values);
		const response = await chand.get('/offers', { params: { requestId: values } });
		handleResponse(response,
			(data) => {
				if (data.offers.length > 0) {
					const offers = data.offers.map(offer => ({
						id: offer.id || offer.offer_id,
						title: offer.title,
						description: offer.description,
						price: offer.price,
					}));
					dispatch({
						type: ActionTypes.BID_ALL,
						payload: {
							all: offers,
							loading: false,
							requestId: values,
							pagination: data.pagination,
						},
					});

					NavigationService.navigate('RequestFeedBack');
				} else {
					alert('پیشنهادی یافت نشد', 'پیام');
				}
			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.BID_ALL, payload: { error: errorMessage } });
			},
		);
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(offerSet({ loading: false, error: errorMessage }));
		});
	}
};

export const offerMore = ({ offerPage }) => async (dispatch, getState) => {
	dispatch(requestSet({ fetchLoading: true }));
	try {
		const reqId = getState().offer.requestId;

		const response = await chand.get('/offers', {
			params: {
				requestId: reqId,
				page: offerPage,
			},
		});
		handleResponse(response,
			(data) => {
				const offers = data.offers;
				const pagination = data.pagination;
				dispatch({
					type: ActionTypes.BID_MORE, payload: {
						all: [...getState().offer.all.slice(0, offerPage * pagination.size), ...offers],
						fetchLoading: false,
						pagination: data.pagination,
						page: offerPage,
					},
				});

			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.TENDER_ALL, payload: { error: errorMessage } });
			});
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(requestSet({ loading: false, error: errorMessage + e }));
		});
	}
};
export const offerShow = (values) => async (dispatch) => {

	dispatch(offerSet({ loading: true, error: null }));
	try {
		const response = await chand.get(`/offers/${values}`);
		handleResponse(response,
			(data) => {

				dispatch({
					type: ActionTypes.BID_SET,
					payload: {
						offer: data.offer,
						loading: false,

					},
				});
				NavigationService.navigate('OfferShow');
			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.BID_ALL, payload: { error: errorMessage } });
			},
		);
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(offerSet({ loading: false, error: errorMessage }));
		});
	}
};
export const offerReset = () => {
	return {
		type: ActionTypes.BID_RESET,
	};
};
export const orderSet = (values) => {
	return { type: ActionTypes.ORDER_SET, payload: values };
};
export const orderCreate = (values) => async (dispatch) => {
	dispatch(offerSet({ loading: true, error: null, all: [] }));
	try {
		const response = await chand.post(`/orders`, { ...values });
		handleResponse(response,
			() => {

				NavigationService.navigate('OrderSuccess');
			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.BID_ALL, payload: { error: errorMessage } });
			},
		);
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(offerSet({ loading: false, error: errorMessage }));
		});
	}
};
export const orderAll = () => async (dispatch, getState) => {
	dispatch(orderSet({ loading: true }));
	try {
		const response = await chand.get(`/orders`);
		handleResponse(response,
			(data) => {
				dispatch(orderSet({ loading: false, all: data.orders, pagination: data.pagination }));
			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.BID_ALL, payload: { error: errorMessage } });
			},
		);
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(offerSet({ loading: false, error: errorMessage }));
		});
	}
};

export const orderMore = ({ orderPage }) => async (dispatch, getState) => {
	dispatch(requestSet({ fetchLoading: true }));
	try {
		const response = await chand.get('/orders', {
			params: {
				page: orderPage
			},
		});
		handleResponse(response,
			(data) => {
				const orders = data.orders;
				const pagination = data.pagination;
				dispatch({
					type: ActionTypes.ORDER_SET, payload: {
						all: [...getState().order.all.slice(0, orderPage * pagination.size), ...orders],
						fetchLoading: false,
						pagination: data.pagination,
						page: orderPage,
					},
				});

			},
			(errorMessage) => {
				dispatch({ type: ActionTypes.TENDER_ALL, payload: { error: errorMessage } });
			});
	} catch (e) {
		handleError(e, (errorMessage) => {
			dispatch(requestSet({ loading: false, error: errorMessage + e }));
		});
	}
};


const handleResponse = (response, onSuccess, onFailed) => {
	const data = response.data;
	const { status, message } = data.meta;
	if (status === 'ok') {
		onSuccess(data);
	} else {
		let errorMessage = Strings.errorUnknown;
		if (message) {
			errorMessage = typeof message === 'string' ? message : message[Object.keys(message)[0]][0];
		}
		onFailed(errorMessage);
	}
};

const handleError = (error, onError) => {
	console.log({ error });
	let errorMessage = Strings.errorUnknown;
	if (error.response) {
		// The request was made and the server responded with a status code
		// that falls out of the range of 2xx
		const data = error.response.data;
		const { status, message } = data.meta;
		if (message) {
			errorMessage = typeof message === 'string' ? message : message[Object.keys(message)[0]][0];
		}
		onError(errorMessage);
	} else if (error.request) {
		// The request was made but no response was received
		// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
		// http.ClientRequest in node.js
		onError(Strings.errorNetwork);
	} else {
		// Something happened in setting up the request that triggered an Error
		// onError(error.message);
		onError(Strings.errorNetwork);
	}
};
