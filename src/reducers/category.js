import {ActionTypes} from '../actions/types';

const INITIAL_STATE = {
    category: {},
    categoryValid: true,
    categoryModalVisible: false,
    categoryModalHistory: [],
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case ActionTypes.CATEGORY_SET:
            return {...state, ...payload};
        case ActionTypes.CATEGORY_RESET:
            return {...INITIAL_STATE};
        default:
            return state;
    }
}
