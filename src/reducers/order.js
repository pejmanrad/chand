import {ActionTypes} from '../actions/types';

const INITIAL_STATE = {
	all: [],
	loading: false,
	success: false,
	order: {},
	pagination: {
		current: 1,
		total: 0,
	},
	page: 1,
	fetchLoading: false,
};
export default (state = INITIAL_STATE, {type, payload}) => {
	switch (type) {
		case ActionTypes.ORDER_SET:
			return {...state, ...payload};
		case ActionTypes.ORDER_CREATE:
			return {...state, ...INITIAL_STATE, ...payload};
		default:
			return state;
	}
}
