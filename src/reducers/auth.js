import {ActionTypes} from '../actions/types';

const INITIAL_STATE = {
	
	name: '',
	email: '',
	mobile: '',
	securityCode: '',
	phoneValid: true,
	codeValid: true,
	error: null,
	loading: false,
	token: null,
};

export default (state = INITIAL_STATE, {type, payload}) => {
	switch (type) {
		case ActionTypes.AUTH_SET:
			return {...state, ...payload};
		case ActionTypes.AUTH_SIGN_IN:
			return {...state, ...INITIAL_STATE, ...payload};
		case ActionTypes.AUTH_SIGN_IN_CONFIRM:
			return {...state, INITIAL_STATE, loading: false, ...payload};
		case ActionTypes.AUTH_SIGN_OUT:
			return {...state, ...INITIAL_STATE, ...payload};
		default:
			return state;
	}
}
