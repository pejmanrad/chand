import {ActionTypes} from '../actions/types';
const INITIAL_STATE={
    loading:false,
    fullName:'',
    image:null,
}
export default (state = INITIAL_STATE, {type,payload}) => {
    switch (type) {
        case 'FETCH_USER':
            return {...state, ...payload};
            case ActionTypes.USER_EDIT:
                return {...state,...INITIAL_STATE,...payload}
        default:
            return state;
    }
}