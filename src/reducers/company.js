import {ActionTypes} from '../actions/types';

const INITIAL_STATE = {
    currentStep: 0,
    companyType: 1,
    name: '',
    postalCode: '',
    bankAccountNumber: '',
    nameValid: true,
    postalCodeValid: true,
    bankAccountNumberValid: true,
    natural: {
        tradeUnionId: '',
        businessLicenseNumber: '',
        tradeUnionIdValid: true,
        businessLicenseNumberValid: true,
        businessLicenseFile: null,
        businessLicenseUploading: null,
        businessLicenseUploadingProgress: 0,

    },
    legal: {
        registerationNumber: '',
        nationalId: '',
        registerationNumberValid: true,
        nationalIdValid: true,
    },
    loading: false,
    error: null,
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case ActionTypes.COMPANY_SET:
            return {...state, ...payload};
        case ActionTypes.COMPANY_GET:
            return {...state, ...INITIAL_STATE, ...payload};
        case ActionTypes.COMPANY_ALL:
            return {...state, ...INITIAL_STATE, ...payload};
        case ActionTypes.COMPANY_CREATE:
            return {...state, ...INITIAL_STATE, ...payload};
        case ActionTypes.COMPANY_RESET:
            return {...INITIAL_STATE};
        default:
            return state;
    }
}