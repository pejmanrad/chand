import {ActionTypes} from '../actions/types';

const INITIAL_STATE = {
	categories: [],
	appInfo:{
		isUpdate:true
	},
	modalShow:true,
	route:'auth'
};

export default (state = INITIAL_STATE, {type, payload}) => {
	switch (type) {
		case ActionTypes.GENERAL_SET:
			return {...state, ...payload};
		default:
			return state;
	}
}
