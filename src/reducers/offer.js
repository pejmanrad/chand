import {ActionTypes} from '../actions/types';

const INITIAL_STATE = {

    all: [],
    offer:{},
    pagination:{
        current: 1,
        total:0
    },
    page:1,
    loading: false,
    fetchLoading:false,
    error: null,
    requestId:0
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case ActionTypes.BID_SET:
            return {...state, ...payload};
        case ActionTypes.BID_ALL:
            return {...state, ...payload};
        case ActionTypes.BID_MORE:
            return {...state, ...INITIAL_STATE, ...payload};
        case ActionTypes.BID_CREATE:
            return {...state, ...INITIAL_STATE, ...payload};
        case ActionTypes.BID_RESET:
            return {...INITIAL_STATE};
        default:
            return state;
    }
}
