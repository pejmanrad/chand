import {ActionTypes} from '../actions/types';

const INITIAL_STATE = {
	
	all: [],
	requestType: 1,
	image: require('../../assets/images/question.png'),
	title: '',
	titleValid: true,
	description: '',
	descriptionValid: true,
	categoryValid:true,
	expiresAt: '',
	expiresAtValid: true,
	opensAt: '',
	opensAtValid: true,
	requestFormFile: null,
	requestFormUploading: null,
	requestFormUploadingProgress: 0,
	publicRequest: {},
	limitedRequest: {
		companies: [],
	},
	pagination:{
		current: 1,
		total:0
	},
	currentStep: 0,
	loading: false,
	fetchLoading:false,
	page:1,
	error: null,
};

export default (state = INITIAL_STATE, {type, payload}) => {
	switch (type) {
		case ActionTypes.TENDER_SET:
			return {...state, ...payload};
		case ActionTypes.TENDER_ALL:
			return {...state, ...INITIAL_STATE, ...payload};
		case ActionTypes.TENDER_MORE:
			return {...state,...INITIAL_STATE,...payload};
		case ActionTypes.TENDER_CREATE:
			return {...state, ...INITIAL_STATE, ...payload};
		case ActionTypes.TENDER_RESET:
			return {...INITIAL_STATE};
		default:
			return state;
	}
}
