import {combineReducers} from 'redux';
import general from './general';
import category from './category';
import auth from './auth';
import company from './company';
import order from './order'
import user from './user';
import request from './request';
import offer from './offer';
export default combineReducers({
    general,
    category,
    auth,
    company,
    request,
    offer,
    user,
    order
});
