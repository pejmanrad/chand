import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
	ActivityIndicator,
	Image,
	ImageBackground,
	SafeAreaView,
	BackHandler,
	StyleSheet,
	View,
	Text,
} from 'react-native';
import SInfo from 'react-native-sensitive-info';
import { Colors, Dimensions, Strings } from '../values';
import { HamraaText } from '../components/commons';
import { authSet, Base, generalSet } from '../actions';

class SplashScreen extends Component {

	constructor(props) {
		super(props);
		this.state = {
			startTime: Date.now(),
			isVisible: true,
		};
	}

	componentDidMount() {
		this.loadApp().catch();
	}

	loadApp = async () => {

		let token = null;


		try {
			await this.props.Base();
			token = await SInfo.getItem('token', {});
			if (token) {
				this.props.authSet({
					token,
				});
			}

		} catch (e) {
			console.log({ e });
		} finally {
			const route = token ? 'Main' : 'Auth';
			this.onAppLoaded(3000 - (Date.now() - this.state.startTime), route);
		}
	};

	onAppLoaded = (delay, route) => {
		if (this.props.appInfo.isUpdate) {
			setTimeout(() => {
				this.props.navigation.navigate(route);
			}, delay);
		} else {
			console.log(route)
			this.props.generalSet({ route });
		}
	};
	ignoreUpdate = () => {
		this.props.navigation.navigate(this.props.route);
	}
	renderUpdate = () => {
		if (this.props.appInfo.isUpdate) {
			return (
				<ImageBackground
					style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'space-around' }}
					source={require('../../assets/splash.png')}>


					<View>
						<Image source={require('../../assets/icon_round.png')} style={styles.image} />
						<HamraaText style={styles.text}> {Strings.appNameFa} </HamraaText>
					</View>
					<ActivityIndicator color={Colors.primaryLight} />
				</ImageBackground>
			);
		} else {
			return (
			<View style={styles.modal}>
				<Text style={styles.modalTitle}>ورژن جدیدی از برنامه وجود دارد</Text>
				{this.props.appInfo.forceUpdate ? <Text onPress={() => BackHandler.exitApp()} style={[, styles.modalButton, { left: 15 }]}>خروج</Text> :
					<Text style={[styles.modalButton, { left: 15 }]} onPress={() => this.ignoreUpdate()}>نادیده بگیر</Text>
				}
				<Text style={[styles.modalButton, { right: 15 }]}>به روز رسانی</Text>
			</View>
			)
		}

	};

	render() {
		const { modalShow, appInfo } = this.props;
		return (
			<SafeAreaView style={styles.container}>
				<View style={{
					flex: 1,
					backgroundColor: 'transparent',

				}}>
						{this.renderUpdate()}

				</View>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.primaryDark,
	},
	modal: {
		flexDirection: 'column',
		flex: 1,
		backgroundColor: 'white',
		margin: 30,
		borderRadius: 5,
		padding: 10,
	},
	modalTitle: {
		fontWeight: 'bold', textAlign: 'center',
		fontSize: 18
	},
	modalButton: {
		padding: 7, borderRadius: 3, bottom: 15, position: 'absolute', borderWidth: 1, borderColor: '#ccc'
	},
	backgroundImage: {
		position: 'absolute',
		top: 0,
		left: 0,
		width: '100%',
		height: '100%',
	},

	image: {
		height: 100,
		width: 100,
	},

	text: {
		color: 'white',
		textAlign: 'center',
		fontFamily: 'IranYekanBold',
		fontSize: Dimensions.fontXLarge,
		marginBottom: 16,
	},
});
const mapStateToProps = ({ general }) => {
	return { ...general };
};

export default connect(
	mapStateToProps,
	{ authSet, Base, generalSet },
)(SplashScreen);
