import React, {Component} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {HamraaButton, HamraaText,  HamraaTextInputLinear} from '../components/commons';
import {Colors, Dimensions, Strings} from '../values';
import {authSet, signInConfirm} from '../actions';
import {validateCode} from '../utils/validators';
import LinearGradient from 'react-native-linear-gradient';
import {Image} from 'react-native-elements';

class SigninConfirmScreen extends Component {
	
	handleSignInConfirm = () => {
		
		const validateCode = this.validateCode();
		if (validateCode) {
		
			this.props.signInConfirm({securityCode: this.props.securityCode,mobile:this.props.mobile/*, password: this.props.password*/});
		}
	};
	
	validateCode = () => {
		const {securityCode} = this.props;
		const codeValid = validateCode(securityCode);
		this.props.authSet({codeValid});
		codeValid || this.emailInput.shake();
		return codeValid;
	};
	
	
	renderError() {
		const {error} = this.props;
		if (error) {
			return (
				<HamraaText>{error}</HamraaText>
			);
		}
		return null;
	}
	
	render() {
		const {
			loading,
			securityCode,
			codeValid,
			authSet,
			error,
		} = this.props;
		
		return (
			<SafeAreaView style={styles.container}>
				<LinearGradient
					// start={{x: 1, y: 0}}
					// end={{x: 0.2, y: 0}}
					colors={Colors.primaryGradient}
					style={styles.container}
				>
					<ScrollView
						keyboardShouldPersistTaps="handled"
						contentContainerStyle={styles.containerContent}
					>
						<Image
							source={require('../../assets/icon_round.png')}
						/>
						<View style={{width: '80%', alignItems: 'center'}}>
							<HamraaTextInputLinear
								inputRef={input => this.emailInput = input}
								icon="expeditedssl"
								iconSize={35}
								label={Strings.titleCode}
								value={securityCode}
								onChangeText={securityCode => authSet({securityCode})}
								placeholder={Strings.hintCode}
								keyboardType="numeric"
								returnKeyType="next"
								errorMessage={
									codeValid ? null : Strings.errorInvalidode
								}
								onSubmitEditing={() => {
									this.validateCode();
								}}
							/>
							{this.renderError()}
						</View>
						<View style={{width: '80%', alignItems: 'center'}}>
							<View style={styles.buttonsContainer}>
								<HamraaButton
									title={Strings.titleSignInConfirm}
									loading={loading}
									onPress={this.handleSignInConfirm}/>
							</View>
						
						</View>
					
					</ScrollView>
				</LinearGradient>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	containerContent: {
		flexGrow: 1,
		paddingBottom: 20,
		paddingTop: 20,
		alignItems: 'center',
		justifyContent: 'space-around',
	},
	buttonsContainer: {
		width: '80%',
		alignItems: 'stretch',
	},
	optionsContainer: {
		alignSelf: 'stretch',
		justifyContent: 'space-around',
		flexDirection: 'row-reverse',
		marginVertical: 16,
	},
	optionsItem: {},
});

const mapStateToProps = ({auth}) => {
	return {...auth};
};

export default connect(
	mapStateToProps,
	{authSet, signInConfirm},
)(SigninConfirmScreen);
