import React, {Component} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
// import Steps from 'react-native-steps';
// import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import RequestBaseInfo from '../components/requests/RequestBaseInfo';
import RequestPreview from '../components/requests/RequestPreview';
import RequestDocsUpload from '../components/requests/RequestDocsUpload';
import {Colors, Strings} from '../values';
import {requestCreate, requestSet} from '../actions';
import {HamraaButton, HeaderGradiant} from '../components/commons';
import {Icon} from 'react-native-elements';
import category from '../reducers/category';
import {validateString} from '../utils/validators';

const STEPS_COUNT = 3;

class RequestCreateScreen extends Component {
	
	validateTitle = () => {
		const {title} = this.props;
		const titleValid = validateString(title,4,100);
		this.props.requestSet({titleValid});
		titleValid || this.titleInput.shake();
		return titleValid;
	};
	validateDescription = () => {
		const {description} = this.props;
		const descriptionValid = validateString(description,4,500);
		this.props.requestSet({descriptionValid});
		descriptionValid || this.descriptionInput.shake();
		return descriptionValid;
	};
	validateCategory = () => {
		const category = this.props.category.title;
		let categoryValid=false;
		if(category) {
			categoryValid=true;
			this.props.requestSet({categoryValid:true});
		}
		categoryValid || this.categoryInput.shake();
		return categoryValid;
	};
	setRefs = ({titleInput, descriptionInput,categoryInput}) => {
		if (titleInput) {
			this.titleInput = titleInput;
		}
		if (descriptionInput) {
			this.descriptionInput = descriptionInput;
		}
		if(categoryInput){
			this.categoryInput=categoryInput
		}
	};
	
	handleBefore = () => {
		const {currentStep, requestSet} = this.props;
		if (currentStep > 0) {
			requestSet({currentStep: currentStep - 1});
		}
	};
	
	handleNext = () => {
		const {currentStep, requestSet} = this.props;
		
		if (this.validateTitle()&&this.validateDescription()&&this.validateCategory()) {
			if (currentStep < STEPS_COUNT - 1) {
				requestSet({currentStep: currentStep + 1});
			}
		}
	};
	
	handleOK = () => {
		const {requestCreate, title, description, category, requestFormFile} = this.props;
		console.log('handle ok '+requestFormFile);
			requestCreate({
			title,
			description,
			categoryId: category.id,
			image: requestFormFile,
		});
	};
	
	renderContent() {
		switch (this.props.currentStep) {
			case 0:
				return <RequestBaseInfo setRefs={this.setRefs}/>;
			case 1:
				return <RequestDocsUpload/>;
			case 2:
				return <RequestPreview/>;
			
			default:
				return null;
		}
	}
	
	renderBottom() {
		
		if (this.props.currentStep > 1) {
			return (
				<View style={styles.containerBottom}>
					<HamraaButton
						icon="check"
						title={Strings.titleOK}
						onPress={this.handleOK}
						loading={this.props.loading}
						containerStyle={styles.button}
						buttonStyle={{backgroundColor: Colors.green}}
					/>
					<HamraaButton
						iconRight
						icon="chevron-right"
						title={Strings.titleBefore}
						onPress={this.handleBefore}
						containerStyle={styles.button}
					/>
				</View>
			);
		}
		
		if (this.props.currentStep > 0) {
			return (
				<View style={styles.containerBottom}>
					<HamraaButton
						icon="chevron-left"
						title={Strings.titleNext}
						onPress={this.handleNext}
						containerStyle={styles.button}
					/>
					<HamraaButton
						iconRight
						icon="chevron-right"
						title={Strings.titleBefore}
						onPress={this.handleBefore}
						containerStyle={styles.button}
					/>
				</View>
			);
		}
		
		return (
			<View style={styles.containerBottom}>
				<HamraaButton
					icon="chevron-left"
					title={Strings.titleNext}
					onPress={this.handleNext}
					containerStyle={styles.button}
				/>
			</View>
		);
	}
	
	render() {
		
		return (
			<SafeAreaView style={styles.container}>
				<ScrollView
					keyboardShouldPersistTaps="handled"
					contentContainerStyle={styles.contentContainer}
				>
					<HeaderGradiant
						title='ثبت درخواست'
						rightIcon={<Icon name='menu' underlayColor={'transparent'} type='MaterialIcons' color='#fff' size={26}/>}
						leftIcon={<Icon name='chevron-left' underlayColor={'transparent'} type='MaterialIcons' color='#fff' size={26}
											 onPress={() => this.props.navigation.goBack()}/>}
					/>
					{this.renderContent()}
					
					
					{this.renderBottom()}
				</ScrollView>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	contentContainer: {
		flexGrow: 1,
		justifyContent: 'space-between',
	},
	containerTop: {
		paddingTop: 16,
	},
	containerBottom: {
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	button: {
		width: '50%',
		marginHorizontal: 16,
	},
});

const mapStateToProps = ({request, category}) => {
	return {...request, ...category};
};

export default connect(
	mapStateToProps,
	{ requestCreate,requestSet},
)(RequestCreateScreen);
