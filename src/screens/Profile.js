import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import SInfo from 'react-native-sensitive-info'
import { Text, Icon, Avatar, Overlay, Button } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../values'
class Profile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isVisible: false
		}
	}
	LogOff = () => {
		SInfo.deleteItem('token', {});
		this.props.navigation.navigate('Auth');
	};
	render() {
		return (<View style={styles.container}>
			<Overlay isVisible={this.state.isVisible}
				overlayBackgroundColor={'white'} height='auto' width='auto'
				onBackdropPress={() => this.setState({ isVisible: false })}>
				<View>
					<Text style={{ margin: 20 }} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>مایل به خارج شدن از برنامه هستنید؟</Text>
					<Button title="خروج" onPress={() => this.LogOff()} buttonStyle={{ backgroundColor: '#ff797c' }} titleStyle={{ color: '#fff' }} type="clear" />
				</View>
			</Overlay>
			<LinearGradient
				colors={Colors.profileGradient}
				style={styles.container}
			>
				<View style={{ width: '100%', padding: 10, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', }}>
					<Icon name='chevron-left' underlayColor={'transparent'} activeOpacity={.9} type='MaterialIcons' color='#988' size={30} onPress={() => this.props.navigation.goBack()} />
					<Icon name='power-settings-new' underlayColor={'transparent'} activeOpacity={.9} type='MaterialIcons' color='#988' size={30} onPress={() => this.setState({ isVisible: true })} />

				</View>
				<View style={styles.content}>
					<View style={styles.profilePack}>
						<Avatar rounded
							size="large"
							title="BP"
							activeOpacity={.8}
							// showEditButton
							source={require('../../assets/images/avatar_default.png')} />
						<Text style={{ color: '#40545f', marginTop: 7 }}>
							نام و نام خانوادگی
						</Text>
						<Text style={{ color: '#40545f', marginTop: 7 }}>
							تلفن همراه : 09195888888
						</Text>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('EditProfile')} style={{ flexDirection: 'row', marginTop: 10 }}>
							<Icon
								name='user-o'
								type='font-awesome'
								size={20}
								color='gray' />
							<Text style={{ paddingLeft: 5, color: 'gray' }}>edit</Text>
						</TouchableOpacity>
					</View>
					<View style={styles.payment}>
						<Text style={styles.listHeader}>لیست پرداخت ها</Text>
						<View style={styles.listPayment}>
							<View style={{ width: '100%', padding: 10, flexDirection: 'row', borderBottomColor: '#cee4ef', borderBottomWidth: 2 }}>
								<View style={{ width: '50%', alignItems: 'flex-start' }}>
									<Text style={{ color: '#40545f' }}>100000</Text>
								</View>
								<View style={{ width: '50%', alignItems: 'flex-end' }}>
									<Text style={{ color: '#40545f' }}>عنوان خرید</Text>

								</View>
							</View>
						</View>
					</View>
				</View>

			</LinearGradient>
		</View>);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	content: {
		flex: 1,
		justifyContent: 'flex-start',
		alignItems: 'center',
	},
	profilePack: {
		justifyContent: 'space-around',
		alignItems: 'center',
		flexDirection: 'column'
	},
	payment: {
		width: '100%',
		flexDirection: 'column',
		justifyContent: 'flex-start',
		marginTop: 20,
		flex: 1
	},
	listHeader: {
		textAlign: "right",
		paddingRight: 20,
		fontSize: 16,
		color: '#40545f'
	},
	listPayment: {
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'flex-start',
		paddingVertical: 10,
		backgroundColor: '#83bdc9',
		borderTopLeftRadius: 15,
		borderTopRightRadius: 15,
		marginLeft: 10, marginRight: 10,
		marginTop: 10,
		flex: 1,
		padding: 10
	}
});
export default Profile;