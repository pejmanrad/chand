import React from 'react'
import { Text, View, StyleSheet , Button,Alert} from 'react-native'
import FS from 'react-native-fs'
class Test extends React.Component {
   
    func=()=>{
       
        var uploadUrl = 'http://epob.ir/gateway/api/v1/requests'; 
        var files = [
            {
              name: 'image',
              filename: 'forUpload.jps',
              filepath:   require('../../assets/images/forUpload.jpg'),
              filetype: 'image/jpg'
            },
          ];
         var uploadBegin = (response) => {
            var jobId = response.jobId;
            console.log('UPLOAD HAS BEGUN! JobId: ' + jobId);
          };
          
          var uploadProgress = (response) => {
            var percentage = Math.floor((response.totalBytesSent/response.totalBytesExpectedToSend) * 100);
            console.log('UPLOAD IS ' + percentage + '% DONE!');
          };
          FS.uploadFiles({
            toUrl: uploadUrl,
            files: files,
            method: 'POST',
            headers: {
              'Accept': 'application/json',
            },
            fields: {
              'title': 'upload',
              'categoryId':1,

            },
            begin: uploadBegin,
            progress: uploadProgress
          }).promise.then((response) => {
              if (response.statusCode == 200) {
                console.log('FILES UPLOADED!'); // response.statusCode, response.headers, response.body
              } else {
                console.log('SERVER ERROR');
              }
            })
            .catch((err) => {
              if(err.description === "cancelled") {
                // cancelled by user
              }
              console.log(err);
            });
            console.log('hi')
    }
    render() {
        return (<View style={styles.container}>
            <Text>hi</Text>
            <Button title='upload' onPress={this.func}/>
        </View>)
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
export default Test;