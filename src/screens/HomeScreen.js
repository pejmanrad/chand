import React, {Component} from 'react';
import {connect} from 'react-redux';
import {SafeAreaView, StyleSheet, View, Text} from 'react-native';
import CustomerDashboard from '../components/customers/CustomerDashboard';
import {HeaderGradiant} from '../components/commons/HamraaHeaderGradiant';
import {Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {Colors} from '../values';
import GridView from '../components/commons/GridView';
class HomeScreen extends Component {
	
	constructor() {
		super();
	}
	
	render() {
		return (
			<SafeAreaView style={styles.container}>
				
				<View style={styles.containerMain}>
					<HeaderGradiant
						title='انتخاب دسته بندی'
						rightIcon={<Icon name='menu' underlayColor={'transparent'} type='MaterialIcons' color='#fff' size={26}/>}
						leftIcon={<Icon name='chevron-left' underlayColor={'transparent'} type='MaterialIcons' color='#fff' size={26}
											 onPress={() => this.props.navigation.goBack()}/>}
					/>
					<GridView/>


				
				</View>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// paddingTop: 8,
	},
	containerMain: {
		flex: 1,
		backgroundColor:'#08a4d1'
		// justifyContent: 'space-between',
		
	},
	GradiantStyle: {
		flex: 1,
	},
});

const mapStateToProps = ({auth}) => {
	return {...auth};
};

export default connect(
	mapStateToProps,
)(HomeScreen);
