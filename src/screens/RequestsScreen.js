import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { HeaderGradiant, PRList } from '../components/commons';
import { requestAll, requestMoreData, offerAll, requestSet } from '../actions';
import { Icon } from 'react-native-elements';
import { Item } from '../components/requests/ListItem';
import { baseURL } from '../apis/chand';

class RequestsScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fetching_status: false,
			isLoading: false
		};
	}
	componentDidMount() {
		this.props.requestAll();
	}
	showDetails = (requestId) => {
		this.props.offerAll(requestId)
	};

	moreData = () => {
		const { pagination, loading, all, page } = this.props;
		if (!loading && (all.length <= 0 || pagination.current < pagination.last)) {
			const reqPage = page + 1;
			this.props.requestMoreData({ reqPage });
		}
	};

	render() {
		const { all, loading, error, fetchLoading, image } = this.props;
		return (
			<View style={styles.container}>
				<HeaderGradiant
					title='لیست درخواست'
					rightIcon={<Icon name='menu' underlayColor={'transparent'} type='MaterialIcons' color='#fff' size={26} />}
					leftIcon={<Icon name='chevron-left' underlayColor={'transparent'} type='MaterialIcons' color='#fff' size={26}
						onPress={() => this.props.navigation.goBack()} />}
				/>

				<View style={styles.Main}>

					{loading ? <ActivityIndicator size={'large'} /> : null}
					<PRList
						data={all}
						listFooter={(!loading&&fetchLoading) ? fetchLoading : false}
						endReached={this.moreData}
						threshold={.5}
						renderItem={({ item }) => {
							return (
								<Item
									active={(item.status.name === 'pending') ? true : false}
									badge={item.offersCount}
									title={item.title}
									MaxPrice={item.maxPrice}
									MinPrice={item.minPrice}
									ImageUri={item.image ? { uri: `${baseURL}/${item.image}` } : this.props.image}
									onItemPress={() => this.showDetails(item.id)}
								/>
							)
						}
						}
					/>
				</View>

			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	Main: {

		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'center',
		flex: 1
	},
	footerStyle: {
		padding: 7,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopWidth: 2,
		borderTopColor: '#009668',

	},
	touchableStyle: {
		padding: 7,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#f44336',
		borderRadius: 5
	},
	insideText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: 18
	}
});

const mapStateToProps = ({ request }) => {
	return { ...request };
};

export default connect(
	mapStateToProps,
	{ requestAll, requestMoreData, offerAll },
)(RequestsScreen);
