import React from 'react';
import { View, Text, StyleSheet, ScrollView, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { HeaderGradiant, PRList } from '../components/commons';
import { Icon } from 'react-native-elements';
import { orderAll, orderMore } from '../actions';
import { OrderItem } from '../components/orders/OrderItem';

class OrdersScreen extends React.Component {
	componentDidMount() {
		this.props.orderAll();
	}

	moreData = () => {

		const { pagination, loading, all, page } = this.props;

		if (!loading && (all.length <= 0 || pagination.current < pagination.last)) {
			console.log('moredata');
			const orderPage = page + 1;
			this.props.orderMore({ orderPage });
		}
	};

	render() {
		const { loading, all, fetchLoading } = this.props;
		console.log(fetchLoading)
		return (
			<View style={styles.container}>
				<HeaderGradiant
					title='لیست سفارش ها'
					rightIcon={<Icon name='menu' underlayColor={'transparent'} type='MaterialIcons' color='#fff' size={26} />}
					leftIcon={<Icon name='chevron-left' underlayColor={'transparent'} type='MaterialIcons' color='#fff' size={26}
						onPress={() => this.props.navigation.goBack()} />}
				/>
				<View style={{
				
					flexDirection: 'column',
					justifyContent: 'flex-start',
					alignItems: 'center',
					flex: 1
				}}>
					{loading ?
						<ActivityIndicator size={'large'} />
						:
						<PRList
							data={all}
							listFooter={fetchLoading}
							endReached={this.moreData}
							threshold={.2}
							renderItem={({ item }) => {
								return <OrderItem title={item.offer.title}
									onItemPress={() => this.props.navigation.navigate('OrderView', {
										item: item
									})}
									status={item.status.name}
								// ImageUri={item.offer.image ? {uri: item.offer.image} : require('../../assets/images/question.png')}
								/>;
							}}
						/>
					}
				</View>
			</View>);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#08a4d1'
	},
});
const mapStateToProps = ({ order }) => {
	return { ...order };
};
export default connect(mapStateToProps, { orderAll, orderMore })(OrdersScreen);
