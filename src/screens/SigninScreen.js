import React, {Component} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {Image} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {HamraaButton, HamraaText, HamraaTextInputLinear} from '../components/commons';
import {Colors, Dimensions, Strings} from '../values';
import {authSet, signIn} from '../actions';
import {validatePhone} from '../utils/validators';


class SigninScreen extends Component {
	
	handleSignIn = () => {
		// this.props.navigation.navigate('SingInConfirm');
		const validatePhone = this.validatePhone();
		if (validatePhone) {
			this.props.signIn({mobile: this.props.mobile,clientType:Strings.clientType});
		}
	};
	
	validatePhone = () => {
		const {mobile} = this.props;
		const phoneValid = validatePhone(mobile);
		this.props.authSet({phoneValid});
		phoneValid || this.emailInput.shake();
		return phoneValid;
	};
	
	
	renderError() {
		const {error} = this.props;
		if (error) {
			return (
				<HamraaText>{error}</HamraaText>
			);
		}
		return null;
	}
	
	render() {
		const {
			loading,
			mobile,
			phoneValid,
			authSet,
			error,
		} = this.props;
		
		return (
			<SafeAreaView style={styles.container}>
				
				<LinearGradient
					// start={{x: 1, y: 0}}
					// end={{x: 0.2, y: 0}}
					colors={Colors.primaryGradient}
					style={styles.container}
				>
					<ScrollView
						keyboardShouldPersistTaps="handled"
						contentContainerStyle={styles.containerContent}
					>
				
						<Image
							source={require('../../assets/icon_round.png')}
						/>
						<View style={{width: '80%', alignItems: 'center'}}>
							<HamraaTextInputLinear
								inputRef={input => this.emailInput = input}
								icon="mobile"
								iconSize={35}
								label={"ورود به سامانه"}
								value={mobile}
								onChangeText={mobile => authSet({mobile})}
								placeholder={"شماره موبایل"}
								keyboardType="numeric"
								returnKeyType="next"
								errorMessage={
									phoneValid ? null : Strings.errorInvalidPhone
								}
								onSubmitEditing={() => {
									this.validatePhone();
								}}
							/>
							{this.renderError()}
						
						</View>
						<View style={{width: '80%', alignItems: 'center'}}>
							<View style={styles.buttonsContainer}>
								<HamraaButton
									title={Strings.titleSignIn}
									loading={loading}
									onPress={this.handleSignIn}/>
							</View>
						</View>
					</ScrollView>
				</LinearGradient>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	containerContent: {
		flexGrow: 1,
		paddingBottom: 20,
		paddingTop: 20,
		alignItems: 'center',
		justifyContent: 'space-around',
	},
	buttonsContainer: {
		width: '80%',
		alignItems: 'stretch',
	},
	optionsContainer: {
		alignSelf: 'stretch',
		justifyContent: 'space-around',
		flexDirection: 'row-reverse',
		marginVertical: 16,
		
	},
	optionsItem: {},
});

const mapStateToProps = ({auth}) => {
	return {...auth};
};

export default connect(
	mapStateToProps,
	{authSet, signIn},
)(SigninScreen);
