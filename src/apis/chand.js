import axios from 'axios';

export const baseURL = 'http://epob.ir/gateway';

const instance = axios.create({
    baseURL: `${baseURL}/api/v1`,
  
    headers: {
        common: {
        },
    }
});

export const setAuthToken = token => {
    instance.defaults.headers.common.Authorization = `Bearer ${token}`;
};

export default instance;
