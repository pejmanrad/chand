import React, {Component} from 'react';
import {StyleSheet, View,StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore,compose} from 'redux';
import thunk from 'redux-thunk';
import {ThemeProvider}from 'react-native-elements'
import reducers from './reducers';
import AppContainer from './AppContainer';
import NavigationService from './NavigationService';
import {setAuthToken} from './apis/chand';

// const store = createStore(
//     reducers,
//     applyMiddleware(thunk)
// );
const middleware=[thunk];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store=createStore(reducers, composeEnhancers(applyMiddleware(...middleware)));
store.subscribe(() => {
    setAuthToken(store.getState().auth.token)
});
const theme={
    Text:{
        style:{
            fontFamily:'IRAN'
        }
    }
}
class App extends Component {
    
    render() {
    
        //barStyle={(Platform.OS === 'android' && Platform.Version >= 23) ? 'dark-content' : 'default'
        return (
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                <View style={styles.container}>
                <StatusBar
						barStyle="light-content"
						hidden={true}
					/>
                    <AppContainer
                        ref={navigatorRef => {
                            NavigationService.setTopLevelNavigator(navigatorRef);
                        }}
                    />
                </View>
                </ThemeProvider>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
     
    },
});

export default App;
