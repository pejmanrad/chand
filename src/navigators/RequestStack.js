import React from 'react';
import {createStackNavigator} from 'react-navigation';
import RequestsScreen from '../screens/RequestsScreen';
import RequestFeedBackScreen from '../components/requests/RequestFeedBack';
import OfferShow from '../components/offers/OfferShow';
import OrderSuccess from '../screens/OrderSuccess';

const RequestStack = createStackNavigator({
	Requests: RequestsScreen,
	RequestFeedBack: RequestFeedBackScreen,
	OfferShow: OfferShow,
	OrderSuccess:OrderSuccess,
}, {
	headerMode: 'none',
	initialRouteName: 'Requests',
});
export default RequestStack;
