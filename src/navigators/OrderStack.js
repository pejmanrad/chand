import React from 'react';
import {createStackNavigator} from 'react-navigation';
import  OrdersScreen from'../screens/OrdersScreen'
import OrderView from '../components/orders/orderView'
const OrderStack = createStackNavigator({
	Orders:OrdersScreen,
	OrderView:OrderView
}, {
	headerMode: 'none',
	initialRouteName: 'Orders',
});
export default OrderStack;
