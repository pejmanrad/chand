import React from 'react';
import {createStackNavigator} from 'react-navigation';
import ProfileScreen from '../screens/Profile';
import Edit from '../components/profiles/Edit';

const ProfileStack =
   createStackNavigator(
   {
       Profile: ProfileScreen,
       EditProfile:Edit
   },
   {
     headerMode:'none',
     initialRouteName:'Profile'
   }
);

export default ProfileStack;
