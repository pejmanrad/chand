import {createBottomTabNavigator, createAppContainer, createStackNavigator} from 'react-navigation';
// import {Icon} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import HomeStack from './HomeStack';
import RequestStack from './RequestStack'
import OrderStack from './OrderStack'
import RequestCreateStack from './RequestCreateStack'
import ProfileStack from './ProfileStack'
import React from 'react';
import Profile from '../screens/Profile'
const TabNavigator = createBottomTabNavigator({
	Home: {
		screen: HomeStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => <Icon name="home" size={35} style={{color: tintColor}}/>,
			
		},
	},
	RequestList: {
		screen: RequestStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => <Icon name="list-ul" size={35} style={{color: tintColor}}/>,
		},
	},
	CreateRequest: {
		screen: RequestCreateStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => <Icon name="plus-circle" size={35} style={{color: tintColor}}/>,
			
		},
	},
	OrderList: {
		screen: OrderStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => <Icon name="align-left" size={35} style={{color: tintColor}}/>,
		},
	},
	Profile: {
		screen: ProfileStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => <Icon name="user-circle-o" size={30} style={{color: tintColor}}/>,
		},
	},
},
	{
	tabBarPosition: 'bottom',
	animationEnabled: true,
	swipeEnabled: true,
	tabBarOptions: {
		showIcon: true,
		activeTintColor: '#ff9000',
		inactiveTintColor: '#ffffff',
		indicatorStyle: {backgroundColor: 'transparent'},
		showLabel: false,
		style: {
			backgroundColor: '#135687',
			height: 50,
		},
	},
});
export default TabNavigator;
