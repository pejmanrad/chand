import {createSwitchNavigator} from 'react-navigation';
import SigninScreen from '../screens/SigninScreen';
import SigninConfrim from '../screens/SigninConfirm';

const AuthSwitch = createSwitchNavigator({
        SignIn: SigninScreen,
        SingInConfirm: SigninConfrim,
    },
    {
        initialRouteName: 'SignIn',
        headerMode: 'none',
    }
);

export default AuthSwitch;
