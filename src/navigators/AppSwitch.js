import React from 'react';
import {createSwitchNavigator} from 'react-navigation';
import AuthSwitch from './AuthSwitch';
import SplashScreen from '../screens/Splash';
import Tabs from './TabNavigator';
import Test from '../screens/Test'
const AppSwitch = createSwitchNavigator({
        Splash: SplashScreen,
        Main: Tabs,
        Auth: AuthSwitch,
        Test:Test
    },
    {
        initialRouteName: 'Splash',
    }
);
export default AppSwitch;

