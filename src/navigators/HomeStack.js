import React from 'react';
import {createStackNavigator} from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import RequestCreateScreen from '../screens/RequestCreateScreen';

const HomeStack =
   createStackNavigator(
   {
       Home: HomeScreen,
   },
   {
     headerMode:'none'
   }
);

export default HomeStack;
