import React from 'react';
import {createStackNavigator} from 'react-navigation';
import RequestCreateScreen from '../screens/RequestCreateScreen';

const RequestCreateStack =
	createStackNavigator(
		{
			RequestCreate: RequestCreateScreen,
		},
		{
			headerMode:'none',
			initialRouteName:'RequestCreate'
		}
	);

export default RequestCreateStack;
