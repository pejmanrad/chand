export const validatePhone = (email) => {
    // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const re=/^(09|9)[0-9]{9}$/;
    return re.test(email);
};
export const validateCode=(code)=>{
    const re=/^[0-9]{5}$/;
    return re.test(code);
};
export const validateString=(s,min,max)=>{
    const re=RegExp(`^.{${min},${max}}$`);
    return re.test(s);
};
//
// export const validatePassword = (password) => {
//     const re = /^[a-zA-Z0-9]{4,10}$/;
//     return re.test(password);
//     // return password.length >= 8;
// };
