export const requests = [
    {
        id: '1',
        title: 'پروژه داده کاوی',
        description: 'برخی توضیحات در مورد این پروژه',
        expiresAt: '1397/12/29 10:30',
        opensAt: '1398/01/20 10:30',
        category: 'خدمات',
        price: '320,000,000',
        image: 'https://s100.divarcdn.com/static/thumbnails/1550099198/uOlsGJpBK.jpg',
        thumbnail: 'https://s100.divarcdn.com/static/thumbnails/1550099198/uOlsGJpBK.jpg'
    },
    {
        id: '2',
        title: 'راه اندازی آسانسور',
        description: 'توضیحاتی در مورد این پروژه',
        expiresAt: '1398/01/29 23:30',
        opensAt: '1398/02/07 23:30',
        category: 'فنی',
        price: '86,000,000',
        image: 'https://s100.divarcdn.com/static/pictures/1549976622/O270Irak1.jpg',
        thumbnail: 'https://s100.divarcdn.com/static/thumbnails/1549976622/O270Irak1.jpg'
    },
    {
        id: '3',
        title: 'پروژه داده کاوی',
        description: 'برخی توضیحات در مورد این پروژه',
        expiresAt: '1398/3/15 8:00',
        opensAt: '1398/4/15 8:00',
        category: 'خدمات',
        price: '320,000,000',
        image: 'https://s100.divarcdn.com/static/pictures/1550753318/6l2MchdHm.jpg',
        thumbnail: 'https://s100.divarcdn.com/static/thumbnails/1550753318/6l2MchdHm.jpg'
    },
    {
        id: '4',
        title: 'راه اندازی آسانسور',
        description: 'توضیحاتی در مورد این پروژه',
        expiresAt: '1398/3/15 10:00',
        opensAt: '1398/4/15 10:00',
        category: 'فنی',
        price: '86,000,000',
        image: 'https://s100.divarcdn.com/static/pictures/1549976622/O270Irak1.jpg',
        thumbnail: 'https://s100.divarcdn.com/static/thumbnails/1549976622/O270Irak1.jpg'
    },
    {
        id: '5',
        title: 'پروژه داده کاوی',
        description: 'برخی توضیحات در مورد این پروژه',
        expiresAt: '1398/3/15 12:20',
        opensAt: '1398/4/15 12:20',
        category: 'خدمات',
        price: '320,000,000',
        image: '',
        thumbnail: ''
    },
    {
        id: '6',
        title: 'کاشت 2000 عدد نهال',
        description: 'توضیحاتی در مورد این پروژه',
        expiresAt: '1398/3/15 15:50',
        opensAt: '1398/4/16 15:50',
        category: 'باغبانی و درختکاری',
        price: '480,000,000',
        image: 'https://s100.divarcdn.com/static/pictures/1551686618/1Oo3U0_g6.jpg',
        thumbnail: 'https://s100.divarcdn.com/static/thumbnails/1551686618/1Oo3U0_g6.jpg'
    },
];