import {Dimensions as Dims} from 'react-native';

export const Dimensions = {

    window: {
        width: Dims.get('window').width,
        height: Dims.get('window').height,
    },

    isSmallDevice: Dims.get('window').width < 375,

    tabBarHeight: 65,

    fontXSmall: 10,
    fontSmall: 12,
    fontSmallMedium: 13,
    fontMedium: 14,
    fontMediumLarge: 15,
    fontMediumXLarge: 16,
    fontMediumXXLarge: 17,
    fontLarge: 18,
    fontXLarge: 22,
    fontXLargeXXLarge: 26,
    fontXXLarge: 27,
};
